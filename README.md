Strumenti informatici per la fisica
===================================

This repository hosts the material for the course "Strumenti informatici per la fisica" at the University of Trieste.

The material is available as a [web page](https://coslo.frama.io/sinfo/index.html).

- Bash: [binder link](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fcoslo%2Fsinfo/HEAD?labpath=bash%2F)

**Author**

[Daniele Coslovich](https://www.units.it/daniele.coslovich/)
