.PHONY: all rst docs

all: docs

# Create rst file from source
rst:
	# emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=project.org -f org-rst-export-to-rst --kill
	# cat project.rst | grep -v Author > docs/index.rst
	# rm -f project.rst

	# emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=bash/project.org -f org-rst-export-to-rst --kill
	# cat bash/project.rst | grep -v Author > docs/bash.rst
	# rm -f bash/project.rst

	# emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=python/project.org -f org-rst-export-to-rst --kill
	# cat python/project.rst | grep -v Author > docs/python.rst
	# cp python/*png python/*webp docs/
	# rm -f python/project.rst

	# emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=python/numpy.org -f org-rst-export-to-rst --kill
	# cat python/numpy.rst | grep -v Author > docs/numpy.rst
	# cp python/*png docs/
	# rm -f python/numpy.rst

	# emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=python/intermediate.org -f org-rst-export-to-rst --kill
	# cat python/intermediate.rst | grep -v Author > docs/intermediate.rst
	# rm -f python/intermediate.rst

	# emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=python/faster_python.org -f org-rst-export-to-rst --kill
	# cat python/faster_python.rst | grep -v Author > docs/faster_python.rst
	# cp python/*png docs/
	# rm -f python/faster_python.rst

	emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=git/project.org -f org-rst-export-to-rst --kill
	cat git/project.rst | grep -v Author | sed 's/``\\/``/g' > docs/git.rst
	#rsync -uvaR git/./images/*png docs/
	#rsync -uvaR git/images docs/
	cp git/*png docs/
	cp chart.png networks.png docs/
	rm -f git/project.rst

ipynb:
	orgnb python/*org
	#orgnb python/project.org
	mv python/project.ipynb python/basics.ipynb 

docs: rst ipynb
	make -C docs/ html
