=========================
Intermediate-level Python
=========================




What we'll cover today
----------------------

- **Modules** and **packages**

- Python **scripts** with nice **command-line interfaces**

- More **flexible data structures**

- Basics of **object-orientation** in Python

Modules and packages
--------------------

Modules
~~~~~~~

Suppose you start writing a jupyter notebook for a project of yours and at some point you realize that you need functions or variables you had already defined in another notebook. Resist the temptation to copy and paste! Reusable code can be conveniently stored in **modules** that can be imported with the standard import mechanism.

A module in Python is a simple text file ending in ``.py``. As an example, we put the functions to compute the potential energy of the nano-particle in a file called ``interaction.py``, in the same folder of your notebook

.. code:: python

    def potential(r):
        return 0.0

    def total_potential_energy(position):
        u = potential(1.0)
        return u

.. note::

    It is a good idea to give a meaningful and expressive name to the module. It should describe the overall purpose of the functions defined within it. Remember to follow the `PEP-8 <https://peps.python.org/pep-0008/#package-and-module-names>`_ prescriptions.

Then from your notebook (or any other piece of Python code in the same folder) you can import and use those functions. This is the standard way

.. code:: python

    import interaction

    pos = [[1.0, 1.0, 1.0], [0.0, 0.0, 0.0]]
    interaction.total_potential_energy(pos)

This way makes it clear where the function is coming from. We can also import a specific function

.. code:: python

    from interaction import total_potential_energy

    pos = [[1.0, 1.0, 1.0], [0.0, 0.0, 0.0]]
    total_potential_energy(pos)

or several of them at the same time

.. code:: python

    from interaction import total_potential_energy, potential

**Downside**: importing specific functions or variables may lead to "name clashing" if your code defines a function or variable with the same name. In doubt, better spell out the import module explicitly and keep the function in the module namespace.

It is also possible to import all the objects of a module

.. code:: python

    from interaction import *

but this is **discouraged** because it "`pollutes the namespace <https://stackoverflow.com/questions/2386714/why-is-import-bad>`_". The best practices about imports are crystallized in the `PEP-8 <https://peps.python.org/pep-0008/#imports>`_ as usual.

A useful feature of ``import`` is the ability of changing names dynamically

.. code:: python

    import numpy as np
    from interaction import potential as _potential

You can also **access variables** defined the module, not only functions. Suppose we have a global variable in the module to to select the "backend" used for the potential energy calculation

.. code:: python

    backend = 'fortran'

    def potential_bck(r):
        print(f'backend is {backend}')
        return 0.0

We can change it at run time from the calling Python code

.. code:: python

    import interaction

    interaction.potential_bck(1.0)
    interaction.backend = 'numba'
    interaction.potential_bck(1.0)

::

    backend is fortran
    backend is numba


It all goes well as long as you keep your modules in the **same** folder as your notebook. But what if you want to move the module somewhere else, for instance in a subfolder?

To answer this question we must look closer at the way Python looks for modules in the first place. The key idea is that Python will look for modules in the Python path

.. code:: python

    from pprint import pprint
    import sys

    pprint(sys.path)

::

    ['',
     '/usr/lib/python310.zip',
     '/usr/lib/python3.10',
     '/usr/lib/python3.10/lib-dynload',
     '/home/coslo/teaching/sinfo/env/lib/python3.10/site-packages',
     '~/teaching/sinfo/']



If a module is found in any of the paths included in the above list, then it can be imported in the ways we have seen above. Note the presence of an empty string (``''``), which matches the current folder. The order of search is from the last to the first entry of the list.

.. note::

    Virtual environments like the ones created by ``venv`` crucially redefine the ``path`` variable, to make sure that the environment modules are loaded before the ones of your operating system distribution. try to deactivate the environment and print the ``sys.path`` variable to see the difference.

It is possible to include new paths from the bash shell by modifying the ``PYTHONPATH`` environment variable... but you should not normally need to do that. If all you want is storing several modules in a subfolder, for instance to group them logically, what you actually need is a **package** - read on!

Packages
~~~~~~~~

Python packages provide an extensible mechanism to group several related modules. You can think of a package as a namespace for modules. To make a package, just create a folder with an empty ``__init__.py`` file in it: that's it!

Say we want to call the package ``nanoparticle``. From the terminal

.. code:: sh

    mkdir -p nanoparticle
    touch nanoparticle/__init__.py

Now from Python we can type

.. code:: python

    import nanoparticle

It works but, of course, the package is empty (only default built-in variables are present)

.. code:: python

    dir(nanoparticle)

::

    ['__builtins__', '__cached__', '__doc__', '__file__', '__loader__', '__name__', '__package__', '__path__', '__spec__']


.. note::

    It is very easy to turn a module in a package: just create a folder with the same basename as the module file and copy the module file as ``__init__.py`` inside the package folder! All the ``import`` statements will work just as before.

Packages allow you to better organize your library of functions, possibly in a hierarchical way. Suppose you have created two separate modules

- ``interaction.py:`` functions to compute the total potential energy (possibly in different ways)

- ``potential.py``: functions that implement pairwise potentials (possibly with different functional forms)

This goes in ``nanoparticle/interaction.py``

.. code:: python

    from .potential import lennard_jones as _potential

    def total_potential_energy(position):
        u = _potential(1.0)
        return u

.. note::

    Have you noticed the little dot before ``potential``? This syntax specifies a "relative import", which allows you to refer to modules defined within a package.

This goes in ``nanoparticle/potential.py``

.. code:: python

    def lennard_jones(r):
        return 0.0

Once you have saved these files in the ``nanoparticle`` folder, you can import them like this

.. code:: python

    import nanoparticle.interaction
    import nanoparticle.potential

    nanoparticle.potential.lennard_jones(1.0)
    nanoparticle.interaction.total_potential_energy([])

or like this

.. code:: python

    from nanoparticle.interaction import total_potential_energy
    from nanoparticle.potential import lennard_jones

    lennard_jones(1.0)
    total_potential_energy([])

What happens if you try this? (restart your kernel first!)

.. code:: python

    import nanoparticle

    nanoparticle.potential.lennard_jones(1.0)

Unless you forgot to restart the kernel, you will get

::

    AttributeError: module 'nanoparticle' has no attribute 'potential'

It would be convenient if importing ``nanoparticle`` would automatically add the modules in the namespace. To achieve this, just add the following lines in ``__init__.py``

.. code:: python

    from . import potential
    from . import interaction

restart the kernel and try again.

Further nifty features of Python packages include

- **subpackages** for hierarchical modules' grouping

- **namespace packages** for modular package organization

- **packaging tools** for package distribution (ex. via ``pypi``)

And of course... a huge ecosystem of community-provided open-source packages for all sorts of computational tasks.

Community packages for least-square fitting
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We have already used a couple of community Python packages in previous tutorials, such as ``numpy``, ``numba``, ``f2py-jit``, ... Here, we will have a closer look at packages that perform **least-square fitting**, which is a very common data analysis task in physics. These are a few options, in increasing order of complexity:

- `scipy.stats.linregress <https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.linregress.html>`_ for simple linear regression (i.e., linear least-square fits)

- `numpy.polfyit <https://numpy.org/doc/stable/reference/generated/numpy.polyfit.html>`_ for simple polynomial fits (see however `this link <https://numpy.org/doc/stable/reference/routines.polynomials.html#transitioning-from-numpy-poly1d-to-numpy-polynomial>`_)

- `scipy.optimize.curve\_fit <https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html>`_ for non-linear fitting with arbitrary functions

- `lmfit <https://lmfit.github.io/lmfit-py/>`_ possibly the best fitting Python package, to get full control over fit parameters

In this example, we will use the function ``curve_fit()`` defined in ``scipy.optimize``. Here is how it works to fit an exponential with noise.

.. code:: python

    import numpy
    from scipy.optimize import curve_fit

    def func(x, a, b, c):
        return a * numpy.exp(-b * x) + c
    xdata = numpy.linspace(0, 4, 50)

    y = func(xdata, 2.5, 1.3, 0.5)
    numpy.random.seed(1729)
    y_noise = 0.2 * numpy.random.normal(size=xdata.size)
    ydata = y + y_noise

And here is the fit. The optimized parameters are stored in the ``popt`` array returned by the function

.. code:: python

    import matplotlib.pyplot as plt

    plt.plot(xdata, ydata, 'o', label='data')
    popt, pcov = curve_fit(func, xdata, ydata)
    # Note the *popt syntax unpacks the array into individual variables
    plt.plot(xdata, func(xdata, *popt), '-')

**Exercise**: With strongly varying functions, like an exponential, is better to linearize the data and do a simple linear fit. Try fitting the linearized data and extract ``a``, ``b``. Use a linear least-square fit for that!

Application: extensivity of the energy
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We get back to the question about the extensivity of nano-particle energy, that is, whether :math:`U(N)` scales linearly with :math:`N`. We populate the system with particles on a lattice and crop what is inside a sphere of a given radius

.. code:: python

    import numpy

    def center_of_mass(pos, mass):
        cm = numpy.zeros(pos.shape[1])
        for i in range(pos.shape[0]):
            cm += pos[i, :] * mass[i]
        cm /= sum(mass)
        return cm
    
    def nano_particle(radius, a=1.0):
        M = int(radius / a)
        pos = []
        for i in range(M):
            for j in range(M):
                for k in range(M):
                    pos.append([i*a, j*a, k*a])

        pos = numpy.array(pos)
        mass = numpy.ones(len(pos))
        cm = center_of_mass(pos, mass)
        nano_pos = []
        for _pos in pos:
            if numpy.sum((_pos - cm)**2)**0.5 < radius:
                nano_pos.append(_pos)

        return numpy.array(nano_pos)

Recompile the Fortran source code to compute the potential energy of the system

.. code:: python

    from f2py_jit import jit

    f90 = jit('interaction.f90', flags='-O3 -ffast-math', inline=True)

We loop over a range of radii and compute :math:`U(N)` for each number of particles :math:`N` in the nano-particle. We store the results in two lists

.. code:: python

    N, epot = [], []
    for radius in [3.0, 4.0, 5.0, 6.0, 9.0, 12.0, 18.0, 24.0, 32.0]:
        pos = nano_particle(radius)
        N.append(pos.shape[0])
        epot.append(f90.total_potential_energy_forder(pos.T))
        print(N[-1], epot[-1])

::

    27 -41.177743069944626
    64 -132.18266763384776
    125 -305.13992862805435
    216 -586.4038046481876
    729 -2343.8142723225574
    1728 -6025.815028475831
    5832 -22014.137477018612
    13824 -54252.85349234231
    32768 -132371.01634480487


Have a look at the data...

.. code:: python

    import matplotlib.pyplot as plt

    fig, ax = plt.subplots()
    ax.loglog(N, numpy.abs(list(epot)), '-o')
    ax.loglog(N, 4*numpy.array(N), '-')
    fig.show()

Here we fit the results to a phenomenological model

.. code:: python

    from scipy.optimize import curve_fit

    def func(x, a, b):
        return a*x + b*x**(2/3)

    N, epot = numpy.array(N), numpy.array(numpy.abs(epot))
    popt, pcov = curve_fit(func, N, epot)

    # Note the *popt syntax unpacks the array into individual variables
    plt.loglog(N, epot, 'o')
    Ns = numpy.linspace(N[0], N[-1], 10000)
    fig, ax = plt.subplots()
    ax.loglog(N, epot, 'o')
    ax.loglog(Ns, func(Ns, *popt), '-', label='~N + corr.')
    ax.loglog(Ns, func(Ns, popt[0], 0), '-', label='~N')
    ax.set_xlabel('N')
    ax.set_ylabel('|U|')
    ax.legend()
    fig.show()

It works pretty well!

Scripts
-------

So far we focused on Python syntax and idioms, running pieces of codes directly in Jupyter cells. You may want to run the code as a **script** from the **command line** of your terminal. We'll see now how to do that.

Command line arguments
~~~~~~~~~~~~~~~~~~~~~~

When a Python code has to be executed from the command line, we store the code in a file, typically with a ``.py`` suffix. It is good practice to encapsulate an "entry point" to the script with a ``main()`` function

.. code:: python
    :name: main

    def main(verbose=False):
        if verbose:
            return 'Hello world!'

The ``main()`` function will be called only when the code is executed from the command line

.. code:: python


    if __name__ == '__main__':
        main()

This is not strictly necessary, but it allows us to also treat the file both as a **script** and as a **module**, so that other Python codes can access its functions and variables *without* executing the ``main()`` function.

.. note::

    Your script should not *ask you questions* about input parameters, but just digest you pass to it on the command line. The latter is a more efficient and flexible approach.

Command line interfaces with argh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To parse parameters on the command line and pass them to ``main()`` you can use the ``sys.argv`` list

.. code:: python

    import sys

    print('We passed', len(sys.argv)-1, 'arguments on the command line')
    print(f'The script path is "{sys.argv[0]}"')

.. code:: sh

    python test_script.py bla bla

::

    We passed 2 arguments on the command line
    The script path is "test_script.py"


For simple scripts, this is usually fine but there is a much powerful and simpler alternative! If you want to build a nice command line interface for your Python codes, look no further than the `argh <https://argh.readthedocs.io/en/latest/tutorial.html>`_ package. You can install it from pypi

.. code:: sh

    pip install argh

Use the ``dispatch_command()`` function to create a CLI for your ``main()`` function:

.. code:: python

    def main(verbose=False):
        """Say Hello to the world"""
        if verbose:
            return 'Hello world!'

    if __name__ == '__main__':
        from argh import dispatch_command
        dispatch_command(main)

Save the script as ``main.py``. If you execute it from the command line you will now get a nice help message that explains how to pass parameters to your Python script

.. code:: sh

    python main.py --help

::

    usage: main.py [-h] [-v]

    Say Hello to the world

    options:
      -h, --help     show this help message and exit
      -v, --verbose  False


Print it out

.. code:: sh

    python main.py --verbose

::

    Hello world!


Shut up!

.. code:: sh

    python main.py

We can use this approach to compute the nano-particle energy from the command line. This is an example where one argument is mandatory, while the other is optional.

.. code:: python

    def main(sigma: float, quiet=False):
        """Compute the energy of a nano-particle with radius `sigma`"""
        if not quiet:
            # Pretend we do the actual calculation
            epot = 0.1 * sigma**3
            print(epot)

    if __name__ == '__main__':
        from argh import dispatch_command
        dispatch_command(main)

Notice how we used type annotation (``sigma: float``) to provide a hint of the default type.

Let's loop from the terminal this time

.. code:: sh

    for sigma in 3.0 4.0 5.0; do
        python main_nano.py $sigma
    done

::

    2.7
    6.4
    12.5


This is useful, for instance, if you want to submit jobs on a cluster for long calculations - it is easier to do from the command line!

Dictionaries
------------

Dictionaries are very useful data structures to collect related variables together in a flexible ``struct``-like data structure.

A simple example
~~~~~~~~~~~~~~~~

To illustrate their usage, let's get back to our nano-particle example. We use a dictionary to aggregate the properties of a single particle

.. code:: python

    # Create an empty dictionary
    particle = {}

    # Add keys and values to the dictionary
    particle['position'] = [0.0, 0.0, 0.0]
    particle['species'] = 'Ar'
    particle['mass'] = 1.0

An alternative, more compact syntax to create the dictionary

.. code:: python

    particle = {'position': [0.0, 0.0, 0.0], 'species': 'Ar', 'mass': 1.0}

The syntax to access the **value** associated to a **key** of the dictionary is

.. code:: python

    particle['position']

::

    [0.0, 0.0, 0.0]


We iterate over all the keys and get the corresponding values

.. code:: python

    for key in particle:
        print(f'{key} = {particle[key]}')

::

    position = [0.0, 0.0, 0.0]
    species = Ar
    mass = 1.0


.. tip::

    Use ``sorted(particle)`` to get the dictionary keys as a sorted list!

Arrays of structures vs. structures of arrays
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Our physical system is a composed by particles arranged on the sites of a cubic crystal. We can thus assemble our system as as a *list of dictionaries*

.. code:: python

    # Cells per side
    M = 2
    # Lattice spacing
    a = 1.0

    system = []
    for i in range(M):
        for j in range(M):
            for k in range(M):
                # Build a cubic crystal for simplicity here
                particle = {'position': [i*a, j*a, k*a],
                            'species': 'Ar',
                            'mass': 1.0}
                system.append(particle)

This is a rather natural design for a physical system composed of classical particles, but almost surely not an efficient one. This kind of data layout is called **array of structures** (AoS).

.. code:: python

    system_aos = system

A different approach, which we will ultimately follow for our nano-particle setup, is to use a **structure of arrays** (SoA) layout, instead.

.. code:: python

    system = {'position': [], 'species': [], 'mass': []}
    for i in range(M):
        for j in range(M):
            for k in range(M):
                system['position'].append([i*a, j*a, k*a])
                system['species'].append('Ar')
                system['mass'].append(1.0)
    system_soa = system

Now all the positions are assembled as a single list of lists, and so are species and masses. To access the particle properties

.. code:: python

    print(system_aos[0]['position'])
    print(system_soa['position'][0])

::

    [0.0, 0.0, 0.0]
    [0.0, 0.0, 0.0]


.. tip::

    The choice between `arrays of structures and structures of arrays <https://en.wikipedia.org/wiki/AoS_and_SoA>`_ can have important consequences on the overall design and efficiency of a simulation code!

Objects and classes
-------------------

In the `object-oriented <https://en.wikipedia.org/wiki/Object-oriented_programming>`_ (OO) programming paradigm, objects are a bundle of **variables** ("attributes") and **functions** ("methods") operating on them. The specification of which attributes are attached to a specific object and how methods operate on them is provided by a **class**: individual objects are "instances" of a specific class.

.. note::

    Python takes object-orientation very seriously: almost everything is an object in Python - even functions are...!

To illustrate some basic ideas about OO programming, we'll revisit the design of our system of particles using a class.

.. warning::

    Even though object-orientation is an elegant and widespread programming model, it is not necessarily the way to go for any given computational problem.

Kenneth Reisz `put it down <https://docs.python-guide.org/writing/structure/#object-oriented-programming>`_ nicely:

::

    In summary, pure functions are more efficient building blocks than
    classes and objects for some architectures because they have no
    context or side-effects.

    Obviously, object-orientation is useful and even necessary in many
    cases, for example when developing graphical desktop applications or
    games, where the things that are manipulated [...] have a relatively
    long life of their own in the computer’s memory.

Constructors
~~~~~~~~~~~~

In Python, the ``__init__`` method is used to construct an object (this method is called "constructor" in the OO paradigm).

.. code:: python

    class Particle:

        def __init__(self, position, species='Ar', mass=1.0):
            self.position = position
            # Optional arguments have default values
            self.species = species
            self.mass = mass

The ``self`` variable refers to the specific object we are about to create and whose attributes we want to set or modify. It must be passed as first argument to any method. The name ``self`` is a convention.

We create an object of the ``Particle`` class and access its attributes with the dot syntax (which we already encountered)

.. code:: python

    particle = Particle([0.0, 0.0, 0.0])
    print(particle)
    print(particle.position, particle.species, particle.mass)

::

    <__main__.Particle object at 0x781b2cbf06a0>
    [0.0, 0.0, 0.0] Ar 1.0


.. warning::

    In functions, never use `default values for mutable arguments <https://stackoverflow.com/questions/1132941/least-astonishment-and-the-mutable-default-argument>`_! Assign instead the special object ``None`` as default and deal with the optional argument in the body of the function.

We get a system of particles with **AoS** layout this way

.. code:: python

    M = 10
    system = []
    for i in range(M):
        for j in range(M):
            for k in range(M):
                particle = Particle([i*a, j*a, k*a])
                system.append(particle)

One advantage of classes over the plain dictionary approach is that we can set **default values** of particle properties. Also, accessing particle properties is easier with the dot syntax than via subscripts.

As a first attempt to get a **SoA,** we create a ``System`` class

.. code:: python
    :name: system

    import numpy

    class System:

        def __init__(self, position, species=None, mass=None):
            # The input position is expected to be iterable
            # Make sure it is an array
            self.position = numpy.array(position)
            self.species = species
            self.mass = mass
            # If optional argument are not given, we create them appropriately
            if self.species is None:
                self.species = numpy.array(['Ar'] * len(position))
            if self.mass is None:
                self.mass = numpy.ones(self.position.shape[0])

We create a ``System`` instance and fill in the positions

.. code:: python

    system = System(position=[[0.0, 0.0, 0.0]])
    print(system.position[0], system.species[0], system.mass[0])

::

    [0. 0. 0.] Ar 1.0


.. warning::

    Our current **SoA** approach has a little shortcoming when the number of particles of the system varies. Can you see why? Compare with the **AoS** approach.

Adding class methods
~~~~~~~~~~~~~~~~~~~~

Let's create a ``center_of mass()`` method to compute the CM of the system [Oh boy! `Noweb syntax <https://orgmode.org/manual/Noweb-Reference-Syntax.html>`_ is so nice when writing code documentation with org-mode - `literate programming <https://en.wikipedia.org/wiki/Literate_programming>`_ is awesome. I wish Jupyter could do the same.]

.. code:: python



    def center_of_mass(self):
        cm = numpy.zeros(self.position.shape[1])
        for i in range(self.position.shape[0]):
            cm += self.position[i, :] * self.mass[i]
        cm /= sum(self.mass)
        return cm

.. code:: python

    system = System([[0.0, 0.0, 0.0],
                     [1.0, 1.0, 1.0]])
    print(system.center_of_mass())

::

    [0.5 0.5 0.5]


The ``center_of_mass()`` method looks pretty much like an attribute, but it must be computed *dynamically* because the system attributes may change. You can use `property decorator <https://realpython.com/python-property/>`_ to achieve this.

Exercise
~~~~~~~~

If you want to dive into the OO paradigm, adapt your code to setup the nano-particle using a **SoA** with the class approach. Add the code to setup the lattice and crop the nanoparticle as methods of the class. Think about the **advantages** and **disadvantages** compared to a procedural approach, in which related variables and plain functions are grouped in modules instead. In which situations is the object-oriented approach useful?
