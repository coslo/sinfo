=======================================
Faster Python: numba, f2py and f2py-jit
=======================================




What you'll do today
--------------------

- Learn two key strategies to **accelerate Python code**

- Get some insight into writing **efficient** computational kernels

- Compute efficiently the **total potential energy** of a nano-particle

Organize your code...
---------------------

... in layers

.. image:: code.png

Computational complexity
------------------------

.. danger::

    How does the **number of operations** needed to solve a problem scale with the **size** N of the problem?

Let's consider our nano-particle. The "size of the problem" is simply the number of particles, :math:`N`. How many iterations are required to compute the following two quantities:

**Center of mass**


.. math::

    \vec{r}_{CM} = \frac{\sum_{i=1}^N m_i \vec{r}_i}{\sum_{i=1}^N m_i}

**Total potential energy**


.. math::

    \mathcal{U} = \sum_{i=1}^N \sum_{j>i}^N u(|\vec{r}_i - \vec{r}_i|)

A "good" computational complexity is :math:`O(N)`. If the complexity is polynomial in :math:`N`, say :math:`O(N^2)`, then we may be in trouble. With non-polynomial complexity, say :math:`O(\exp(N))`, we are almost certainly doomed.

.. note::

    Let's consider now a short-range potential, which can be set to zero beyond a cut-off distance: :math:`u(r)=0` if :math:`r>r_c`. What is the computational complexity of total potential energy calculation?

The nano-particle energy as a test bench
----------------------------------------

We want to compute the **total potential energy** of the nano-particle. If we let the particles evolve in time, we would have the so-called **N-body** problem.

To code this, we will write a function that accepts a numpy array ``position`` of shape ``(N, ndim)`` and returns the total potential energy :math:`\mathcal{U}`. It may be more useful to consider :math:`\mathcal{U}/N`.

.. code:: python

    def total_potential_energy(position):
        u = 0.0
        # code here...
        return u

The function ``total_potential_energy`` will call another function that implements the actual potential energy function :math:`u(r)` between a pair of particles. We will assume a Lennard-Jones potential (in reduced units) - it will be easy to change later on

.. code:: python

    def potential(r):
        return 0.0

.. note::

    Is the total potential energy :math:`\mathcal{U}` an extensive function?

Python version
~~~~~~~~~~~~~~

Here is the code

.. code:: python

    def potential(r):
        return 4 * (1/r**12 - 1/r**6)

    def total_potential_energy(position):
        N = position.shape[0]
        u = 0.0
        for i in range(N):
            for j in range(i+1, N):
                rij = position[i, :] - position[j, :]
                r = sum(rij**2)**0.5
                u += potential(r)
        return u

.. warning::

    The square root operation, needed to compute the distance, is much more expensive then other operations such as sum and multiplication. You may want to code the function above by passing the *square distance* as an argument instead.

Here we test the calculation on a set of particles on a large cubic lattice

.. code:: python

    import numpy as np

    M, a = 16, 1.0
    pos = []
    for i in range(M):
        for j in range(M):
            for k in range(M):
                pos.append([i*a, j*a, k*a])
    pos = np.array(pos)

It will take a little while...

.. code:: python

    print(total_potential_energy(pos))

::

    -15160.660070679076
    Elapsed time: 26.69 s

Decorators for timing
^^^^^^^^^^^^^^^^^^^^^

To get the execution time of a function, we can also use the ``timeit`` module, which will repeat the function execution several times. It is not very flexible though.

This is a simple decorator to get the execution time of a function

.. code:: python

    def timeit(func, verbose=True, fmt='.1f', calls=1):
        """
        Decorate function to measure its execution wall time
        """
        def _func(*args, **kwargs):
            import time
            ti = time.time()
            for _ in range(calls):
                res = func(*args, **kwargs)
            tf = time.time()
            # Return the normalized time per call
            dt = (tf-ti) / calls
            print(f'Wall time: {dt:{fmt}} sec')
            return res
        return _func

.. code:: python

    import time
    def f():
        time.sleep(2)
    timeit(f)()

::

    Wall time: 2.0 sec


This is a more flexible decorator

.. code:: python

    import functools
    def timeit(func, minimum_time=1.0, size=1, verbose=True, fmt='.1e', calls=0):
        """
        Decorate function to measure its execution time per unit call, or
        normalized by `(call*size)` if the argument `size` is provided
        (the computational size of the problem).

        The `minimum_time` argument is used to ensure the timing takes no
        less than that time. It defaults to 1 second.
        """
        @functools.wraps(func)
        def _func(*args, **kwargs):
            import time

            # Estimate execution time
            ti = time.time()
            res = func(*args, **kwargs)
            tf = time.time()
            # Now measure execution time
            _calls = calls
            if calls == 0:
                _calls = max(1, int(minimum_time / (tf - ti)))
            ti = time.time()
            for _ in range(_calls):
                res = func(*args, **kwargs)
            tf = time.time()
            # Return the normalized time per unit call/size
            dt = (tf-ti) / (_calls * size)
            if verbose:
                unit = 'sec/call/size' if size > 1 else 'sec/call'
                print(f'Wall time: {dt:{fmt}} {unit} [{func.__name__}]')
            return dt
        return _func

.. code:: python

    timeit(f)()

::

    Wall time: 2.0e+00 sec/call [f]

Numba version
~~~~~~~~~~~~~

One approach to speed up execution of Python code, beyond what numpy can offer, is to translate parts of the code into a lower-level language *at run time* and compile it. This approach is called **just-in-time** (JIT) compilation.

`Numba <https://numba.pydata.org/>`_ is a popular package to compile Python code just-in-time and make it run faster with minimal effort. Install it in your virtual environment with

.. code:: sh

    pip install numba

Import the ``njit`` from ``numba`` and `decorate <https://realpython.com/primer-on-python-decorators/>`_ the function with it

.. code:: python

    from numba import njit

    @njit
    def potential(r):
        return 4 * (1/r**12 - 1/r**6)

    @njit
    def total_potential_energy(position):
        N = position.shape[0]
        u = 0.0
        for i in range(N):
            for j in range(i+1, N):
                rij = position[i, :] - position[j, :]
                r = sum(rij**2)**0.5
                u += potential(r)
        return u

The decorator can also be applied to an existing function

.. code:: python

    def potential(r):
        return 4 * (1/r**12 - 1/r**6)

    potential = njit(potential)

The transformed function is said to be "jitted".

Let's check the time it takes

.. code:: python

    print(total_potential_energy(pos))

::

    -15160.660070679076
    Elapsed time: 1.10 s


**Wow!** That's much faster than the bare Python code with numpy arrays, we gained an order of magnitude.

The second time we execute it, it is even faster (the compilation is done already)

.. code:: python

    print(total_potential_energy(pos))

::

    -15160.660070679076
    Elapsed time: 0.79 s


We can optimize the code a little bit with the ``fastmath`` argument

.. code:: python

    from numba import njit

    @njit(fastmath=True)
    def potential(r):
        return 4 * (1/r**12 - 1/r**6)

    @njit(fastmath=True)
    def total_potential_energy(position):
        N = position.shape[0]
        u = 0.0
        for i in range(N):
            for j in range(i+1, N):
                rij = position[i, :] - position[j, :]
                r = sum(rij**2)**0.5
                u += potential(r)
        return u

.. code:: python

    print(total_potential_energy(pos))

::

    -15160.660070679076
    Elapsed time: 0.76 s


The ``jit()`` decorator offers a `few parameters <https://numba.readthedocs.io/en/stable/user/jit.html>`_ to play with:

- ``nopython`` instructs numba to raise an exception if the code cannot be compiled in ``nopython`` mode, which is faster. Otherwise, numba may fallback to ``object`` compilation mode, which is slower. The ``njit()`` decorator defaults to ``nopython=True``

- ``cache`` avoids recompilation every time you execute the Python program

- ``parallel`` enables automatic parallelization

- ``inline`` controls function inlining

- ``fast_math`` provides more aggressive optimizations

Check out `which Python features <https://numba.pydata.org/numba-doc/dev/reference/pysupported.html>`_ and `which numpy features <https://numba.pydata.org/numba-doc/dev/reference/numpysupported.html>`_ are supported by numba, as well as `additional performance tips <https://numba.readthedocs.io/en/stable/user/performance-tips.html#loop>`_.

Fortran version: f2py
~~~~~~~~~~~~~~~~~~~~~

Numba is great. However, it gives you limited control on how the Python code is translated under the hoods and, in my experience, it does *not* always squeeze all the juice from your code. If you want to have control, get an extra boost of performance and are fine with coding some code in Fortran yourself, read on!

Fortran and Python are a good match for scientific computing. The standard tool to interface Python and Fortran is `f2py <https://numpy.org/doc/stable/f2py/index.html>`_, which is installed by default along with ``numpy``. Note that a working Fortran compiler is required for ``f2py`` to work. Interfacing Python and Fortran with `f2py <https://numpy.org/doc/stable/f2py/index.html>`_ is pretty smooth and works mostly fine with numpy arrays.

Here we code the subroutines in Fortran and store them in a file ``interaction.f90``

.. code:: fortran

    subroutine potential(r, u)
      double precision, intent(in) :: r
      double precision, intent(out) :: u
      u = 4 * (1/r**12 - 1/r**6)
    end subroutine potential

    subroutine total_potential_energy(pos, U)
      double precision, intent(in) :: pos(:,:)
      double precision, intent(out) :: U
      double precision :: rij, r(size(pos,2)), uij
      U = 0.0
      do i = 1,size(pos, 1)
        do j = i+1,size(pos, 1)
          r = pos(i, :) - pos(j, :)
          rij = sum(r**2)**0.5
          call potential(rij, uij)
          U = U + uij
        end do
      end do
    end subroutine total_potential_energy     

.. warning::

    This is not the most efficient way to access multidimensional arrays in Fortran. Since arrays are laid down in memory by column, the inner loop should iterate on the left-most index, to exploit data locality in the cache memory. See further below!

We compile the code from the command line and create a Python module called ``kernels`` (or as you prefer)

.. code:: sh

    f2py -c -m kernels interaction.f90

You will find a file named ``kernels`` dot something in your current directory

.. code:: sh

    ls kernels*

::

    kernels.cpython-310-x86_64-linux-gnu.so


It can be loaded as a module from Python and we can call the functions from Python

.. code:: python

    import kernels

    kernels.potential(1.0)

::

    0.0


Let's check the efficiency of the Fortran version

.. code:: python

    print(kernels.total_potential_energy(pos))

::

    -15160.660070679076
    Elapsed time: 0.48 s


Well.. that's already faster than numba. But wait, we can do better!

.. tip::

    What happens if there is a compilation error? Rather than navigating through the logs of ``f2py``, which are very verbose, compile the code directly from the command line to check the error.

Passing arguments to f2py
^^^^^^^^^^^^^^^^^^^^^^^^^

Interfacing Python and Fortran 90 With `f2py <https://numpy.org/doc/stable/f2py/index.html>`_ is pretty smooth and works mostly fine with numpy arrays - as long as you follow some **basic rules**.

Datatypes must match on Python and Fortran sides, according to this table

.. table::

    +-------------+----------------------------+
    | Numpy types | Fortran types              |
    +=============+============================+
    | float64     | real(8) / double precision |
    +-------------+----------------------------+
    | float32     | real(4) / real             |
    +-------------+----------------------------+
    | int32       | integer(4) / integer       |
    +-------------+----------------------------+
    | int64       | integer(8)                 |
    +-------------+----------------------------+
    | complex64   | complex(4)                 |
    +-------------+----------------------------+
    | complex128  | complex(8)                 |
    +-------------+----------------------------+

Note that Python uses double precision for both float and integer by default. So either on the Fortran side we use double precision integers

.. code:: fortran

    integer(8) :: data

or on the Python side declare integers as ``'int32'``.

.. code:: python

    import numpy
    data = numpy.ones(10, dtype='int32')

On the **Fortran** side:

- declare ``intent``'s of all the arguments of your Fortran subroutines

- declare your floating point variables as ``real(8)`` or ``double precision``, unless you know `how to handles types <https://numpy.org/doc/stable/f2py/python-usage.html>`_

- avoid using Fortran functions, use subroutines with ``intent(out)`` variables as last arguments instead - the results will be returned as result

On the **Python** side:

- use ``dtype=numpy.int32`` for integer arrays, unless you use ``integer(8)`` on the Fortran side (see above)

- *in-place* modification of scalar variables, i.e. ``intent(inout)`` on the Fortran side, requires passing a 0-dimensional array (ex. ``numpy.array(1.0)``)

- shape multi-dimensional numpy arrays using Fortran layout: for instance, ``numpy.ndarray((3, 10), order =`` 'F')= will give the right contiguity in memory layout

Compiling Fortran modules
^^^^^^^^^^^^^^^^^^^^^^^^^

We can also organize subroutines in modules in the Fortran code.

.. code:: fortran

    module potentials
      implicit none
    contains
      subroutine potential(r, u)
        double precision, intent(in) :: r
        double precision, intent(out) :: u
        u = 4 * (1/r**12 - 1/r**6)
      end subroutine potential
    end module potentials

.. code:: sh

    f2py -c -m potentials interaction_module.f90

.. code:: python

    import potentials

    print(potentials.potentials.potential)

::

    <fortran object>

Fortran version: f2py-jit
~~~~~~~~~~~~~~~~~~~~~~~~~

`f2py-jit <https://framagit.org/coslo/f2py-jit>`_ builds efficient Fortran extensions for Python at run time. It extends the machinery of `f2py <https://numpy.org/doc/stable/f2py/>`_ to provide

- **Seamless compilation** of source blocks as Python strings

- **Caching** of module builds across executions

- Optional support for **derived types** via ``f90wrap``

- Optional **inlining** of Fortran routines

Install it in your virtual environment with

.. code:: sh

    pip install f2py-jit

Since ``f2py-jit`` is built on top of ``f2py``, interfacing Python and Fortran follows the same rules. See the `f2py-jit documentation <https://coslo.frama.io/f2py-jit/tutorial/>`_ for more details.

We compile the Fortran code directly from Python

.. code:: python

    from f2py_jit import jit

    f90 = jit('interaction.f90')

The compiled function is readily accessible within the ``f90`` module we just created

.. code:: python

    print(f90.total_potential_energy(pos))

::

    -15160.660070679076
    Elapsed time: 0.48 s


``f2py-jit`` allows you to write Fortran kernels "inline", as a string

.. code:: python

    lennard_jones = """
    subroutine potential(r, u)
      double precision, intent(in) :: r
      double precision, intent(out) :: u
      u = 4 * (1/r**12 - 1/r**6)
    end subroutine potential
    """
    f90 = jit(lennard_jones)
    f90.potential(1.0)

This gives you a lot of **flexibility**: you can stitch together pieces of Fortran code at run time to create a module  dynamically. This would be rather cumbersome in Fortran (ex. via preprocessor).

Optimize the code
^^^^^^^^^^^^^^^^^

We can do even better. Let's squeeze the most performance from our Fortran code by

- adding **optimization flags**

- **inlining** the code

- paying attention to the layout of **multi-dimensional arrays**

.. code:: python

    f90 = jit('interaction.f90', flags='-O3 -ffast-math', inline=True)

.. tip::

    You can check the available optimization flags with ``man gfortran`` and ``man gcc``.

In this case, there is a boost coming from inlining, that is, inserting the subroutine instructions at the place where the subroutine is called. This is courtesy of ``f2py-jit, =f2py`` would not achieve this

.. code:: python

    print(f90.total_potential_energy(pos))

::

    -15160.660070679076
    Elapsed time: 0.23 s


One significant optimization with large multidimensional arrays is following the natural column-major order in the Fortran, that we should pass an (ndim, N) position array so that the inner implicit loop (``pos(:, i) - pos(:, j)``) runs on the left-most index

.. code:: fortran

    subroutine total_potential_energy_forder(pos, U)
      double precision, intent(in) :: pos(:,:)
      double precision, intent(out) :: U
      double precision :: rij, r(size(pos,1)), uij
      U = 0.0
      do i = 1,size(pos, 2)
        do j = i+1,size(pos, 2)
          r = pos(:, i) - pos(:, j)
          rij = sum(r**2)**0.5
          call potential(rij, uij)
          U = U + uij
        end do
      end do
    end subroutine total_potential_energy_forder

.. tip::

    It would be nice if the compiler could do this optimization for us and indeed there is a ``gcc`` flag for that: ``-floop-interchange``. Compile the original Fortran code with that flag and check the result. It may not always work correctly, however, and not always produce the expected result.

It is crucial to pass a transposed array now!

.. code:: python

    f90 = jit('interaction.f90', flags='-O3 -ffast-math', inline=True)
    pos_transpose = pos.T

.. code:: python

    print(f90.total_potential_energy_forder(pos_transpose))

::

    -15160.660070679076
    Elapsed time: 0.07 s


**Boom**! for large arrays the order of the loop is important (this has to do with the size of the **cache memory**).

The **moral of the story**: for large :math:`N`, we could cut down execution time by a factor 300 wrt to the original code and a factor 10 compared to the ``numba`` version.

Conclusions
-----------

- Operations on numpy arrays are **not efficient unless vectorized** over **large array** portions

- This is typically not an issue if the algorithmic **complexity** is :math:`O(N)` or if :math:`N` is small

- The time-consuming kernels that cannot be vectorized should be **compiled**

- **Numba** typically provides a significant performance boost at a **minimal cost**

- For more flexibility and performance, code the kernels in **Fortran** yourself and compile it with ``f2py``

- Use ``f2py-jit`` to get **just-in-time compilation** of Fortran kernels and additional goodies (ex. support for derived types)

Now it is up to you to put everything together and write an efficient code to compute the total interaction energy of a Lennard-Jones nanoparticle!

Projects
--------

**Use f2py or f2py-jit for your computational physics projects**

- write the simulation algorithm in Fortran

- code the high-level interface in Python, via a jupyter notebook

- use ``f2py`` or ``f2py-jit`` as a glue... enjoy the speed!

**Data analysis with numpy and scipy**

- learn how to perform data analysis with numpy and scipy

- implement in Python the statistical analysis (ex. fit)

- report your protocol and workflow in a Jupyter notebook for reproducibility
