===========================
Terminal and bash scripting
===========================




Introduction
------------

Things you'll learn today
~~~~~~~~~~~~~~~~~~~~~~~~~

Use the **terminal** and the **bash** scripting language to

- move around in the **filesystem**, **copy** and **move** files, **create folders**

- **compile** and **execute** a simple fortran program from the command line

- combine commands and type them at the *speed of thought* with a **few tricks**

- automate tasks using **positional arguments**, **loops** and **scripts**

Useful links and docs
~~~~~~~~~~~~~~~~~~~~~

- `GNU Reference manual <https://www.gnu.org/software/bash/manual/bash.htmlReference>`_

- `Recent introductory tutorial (translated in Italian) <https://www.freecodecamp.org/italian/news/shell-scripting-per-principianti/>`_

- `Beginners bash scripting <https://tldp.org/LDP/abs/html/abs-guide.htmlhttps://tldp.org/LDP/Bash-Beginners-Guide/html/Bash-Beginners-Guide.html>`_

- `Advanced bash scripting <https://tldp.org/LDP/abs/html/abs-guide.html>`_

- `Abilità informatiche e telematiche <https://moodle2.units.it/course/view.php?id=6837>`_ (expanded course, bash and Python)

Learn how to learn
~~~~~~~~~~~~~~~~~~

- from the command line: **man page** + ``/word_to_search`` + ``n``, ``p`` + ``Esc``

- from your editor / IDE: **pop up** / **inline help**

- from your mistakes: pay attention and decipher **error messages**

- from the internet: **stack exchange**

- ask your **professor**!

- if all the above fails, ask some artificial intelligence (*really?*)

Prerequisites
~~~~~~~~~~~~~

This tutorial assumes you have access to a terminal (bash shell), a ``gfortran`` compiler and a text editor or integrated development environment (IDE). If not, get access to a Jupyterlab environment `via binder <https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fcoslo%2Fsinfo/HEAD?labpath=bash%2F>`_.

Download `this source file <https://framagit.org/coslo/sinfo/-/raw/master/bash/bottles.f90>`_ and save it on your computer. If you have your terminal open, we can download it directly with `wget <https://manpages.org/wget>`_

.. code:: bash

    wget https://framagit.org/coslo/sinfo/-/raw/master/bash/bottles.f90

Interactive usage
-----------------

Work with files and folders
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Start by listing the contents of the current directory

.. code:: bash

    ls

::

    bottles.f90  project.org  src


Get used to typing this command from time to time, to see what's in the current directory. You can list the content of a given directory with by **passing it as an argument** to ``ls``

.. code:: bash

    ls src

::

    script_0.sh  script_1.sh  script_2.sh


You can get more details about the files by adding the ``-l`` **flag** to the command

.. code:: bash

    ls -l src

::

    total 12
    -rwxrwxr-x 1 coslo coslo 146 Oct  2 20:48 script_0.sh
    -rwxrwxr-x 1 coslo coslo 167 Oct  2 20:48 script_1.sh
    -rwxrwxr-x 1 coslo coslo 419 Oct  2 20:48 script_2.sh


This is actually so common that in most linux distribution the ``ls -l`` command is aliased as ``ll``.

.. note::

    **Flags** are a most natural mechanism to modify a command behavior. They are usually optional. The command's **man page** will give you the authoritative list of available flags.

Inspect the possible flags for ``ls``

.. code:: bash

    man ls

Now create a new directory in the current directory (which could be your home directory or some other landing folder on binder)

.. code:: bash

    mkdir strumenti_informatici

It is a good idea to create separate directories for every lecture. What happens if you do this?

.. code:: bash

    mkdir strumenti_informatici/lezione_1/work

::

    mkdir: cannot create directory ‘strumenti_informatici/lezione_1/work’: No such file or directory


We could create the directory tree, by adding one directory at a time... can we do it in one shot? Yes, by adding the ``-p`` flag to the command, which will create all parent directories.

.. code:: bash

    mkdir -p strumenti_informatici/lezione_1/work

Let's go in the new directory with `cd <https://manpages.org/cd>`_ (change directory)

.. code:: bash

    cd strumenti_informatici/lezione_1/work

The prompt (the left-most part of the command line) should tell you where you are in your filesystem. You can always find it out by typing `pwd <https://manpages.org/pwd>`_

.. code:: bash

    pwd

::

    /home/coslo/teaching/sinfo/bash/strumenti_informatici/lezione_1/work


Let's see how to get back to the directory where we stood at the beginning. There are a few ways to do that:

- ``cd ..`` multiple times

- ``cd ../../../``

If you started from our **home** folder, you could also type

- ``cd ~``

- ``cd`` (!)

.. code:: bash

    cd ../../..

Amazing bash tricks
~~~~~~~~~~~~~~~~~~~

We'll start working - for good now - in the directory we created. Typing again ``cd strumenti_informatici/lezione_1/work`` will work but is tedious. We can use instead a few general purpose **tricks** that make the interaction with the terminal really much faster and smoother.

**Master these tricks and you'll unleash the power of the bash shell!**

.. tip::

    **Trick**: *the terminal remembers the commands you type*

There is a **history** of the commands you typed (stored in ``~/.bash_history)``. Just use the up/down arrows to navigate your history of commands and you will find the command again.

.. tip::

    **Magic trick**: *TAB is you best friend*

The ``tab`` key completes (almost) anything on the command line. Start typing the command you have in mind and press ``tab``: the terminal will complete the path as much as possible. Keeping pressing ``tab`` to get suggestions!

Always keep your little finger close to ``tab`` and get used to press it (it will never hurt!), to complete long paths or commands or whenever you want to get in touch with your new best friend.

.. tip::

    **Mind-blowing trick**: *type commands at the speed of thought with ctrl-r*

Interacting with the terminal can be done at the speed of thought if you learn to use the ``ctrl-r`` shortcut. The best thing is to try it out yourself:

- type ctrl-r then type the beginning of the command you entered before... boom!

- press enter to execute it immediately

- press ``tab`` or the ``arrows`` to edit the command

- you can keep typing ``ctrl-r`` until you hit the command you want.

- ``ctrl-r`` also matches text *within* commands

.. tip::

    **Extra trick:** *move quickly on the command line with shortcuts*

Moving around the command line is greatly facilitated by using the following emacs-like shortcuts

- ``ctrl-a``: go to the beginning of the line

- ``ctrl-e``: go to the end of the line

- ``alt-left``: move left one word at a time

- ``alt-right``: move right one word at a time

- ``ctrl-k``: cut everything that appears on right of the cursor and store it in memory

- ``ctrl-y``: paste the content in memory at the position of the cursor

Compiling and executing a program
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We now use the command ``cp`` to copy the file ``bottles.f90`` from the ``src/`` directory to our work directory

.. code:: bash

    cp bottles.f90 strumenti_informatici/lezione_1/work
    cd strumenti_informatici/lezione_1/work

We can quickly inspect its content with `cat <https://manpages.org/cat>`_

.. code:: bash

    cat bottles.f90

.. code:: f90

    ! Write a verse of a repetitive children's song
    program main
      implicit none
      character(len=32) :: arg = "10"
      character(len=1) :: s = "s"
      integer :: i, stat
      ! Read the first argument on the command line, if present
      if (iargc() == 1) call getarg(1, arg)
      ! Store character variable arg in the integer variable i
      read(arg,*,iostat=stat) i
      if (i == 1) s = ""
      write(*,"(i0,a,a,a)") i, " green bottle", trim(s) ," hanging on the wall"
      write(*,"(a)") "but if one green bottle should accidentally fall"
      write(*,"(a,i0,a)") "there'll be ", i-1, " green bottles hanging on the wall"
    end program main

It is a piece of Fortran code. We can edit this file with a **text editor** or an **integrated development environment** (IDE).

Let's compile the source code

.. code:: bash

    gfortran bottles.f90

Let's check which files were created

.. code:: bash

    ls

::

    a.out  bottles.f90


Let's execute the executable file ``a.out`` in the current directory (hence the ``./`` in front of the file name) we have just created

.. code:: bash

    ./a.out

::

    10 green bottles hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 9 green bottles hanging on the wall


These are the first verses of a popular repetitive `children's song <https://en.wikipedia.org/wiki/Ten_Green_Bottles>`_, probably conceived to teach kids how to count backward (!).

One thing we can do is combining the two commands in one line using a semi-colon

.. code:: bash

    gfortran bottles.f90; ./a.out

If the compilation fails, however, the second command will be attempted anyway. We can execute the second command *only if the first one succeeded* this way

.. code:: bash

    gfortran bottles.f90 && ./a.out

If the first command fails, we can write a message with `echo <https://manpages.org/echo>`_

.. code:: bash

    gfortran bottle.f90 && ./a.out || echo "compilation failed"

::

    f951: Error: Cannot open file ‘bottle.f90’
    : Fatal Error: cannot open input file: bottle.f90
    compilation terminated.
    compilation failed



Suppose we want to change the name of the executable and get rid of the old one. We use a flag for the compiler to specify the executable file

.. code:: bash

    gfortran -o bottles.x bottles.f90
    rm -f a.out

If you want to remove folders, add the ``-r`` flag.

.. danger::

    Be **very** careful when typing ``rm -rf``: you may wipe all your files in one shot! Making backups of your files is important - see the final project proposed `Small project proposals`_!

One nice feature of ``bottles.f90`` is that the number of bottles can be passed as a **command line argument**

.. code:: bash

    ./bottles.x 3

::

    3 green bottles hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 2 green bottles hanging on the wall


Passing arguments to programs and scripts is a great way to **parametrize** them.

Variables and loops
~~~~~~~~~~~~~~~~~~~

To produce the full lyrics of the song, we'll have to execute the program multiple times, using different input arguments. That's something we can automate. Since ``bash`` is a full-fledged programming language, we can use **variables** and **control flow statements** (loops, if tests) to achieve that.

Let's define a variable and print its contents

.. code:: bash

    n=3
    echo $n

::

    3


.. note::

    Variables in bash are **untyped**: you do not need to declare them. They are mostly treated as **character strings**, but depending on the context it is possible to perform arithmetic manipulations on them.

Notice the dollar sign when referencing the variable (a useful alternative in some cases is ``${n}``).

We can also make inline loops with the `for <https://www.gnu.org/software/bash/manual/bash.html#Looping-Constructs>`_ syntax. So now we can start singing the song

.. code:: bash

    for n in 3 2 1; do ./bottles.x $n; done

::

    3 green bottles hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 2 green bottles hanging on the wall
    2 green bottles hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 1 green bottles hanging on the wall
    1 green bottle hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 0 green bottles hanging on the wall


Note the position of the semi-colons!

What if you don't want to type those numbers by hand? The command `seq <https://manpages.org/seq>`_ is handy.

.. code:: bash

    seq 3

::

    1
    2
    3


To count backwards we can pass a negative increment

.. code:: bash

    seq 3 -1 1

::

    3
    2
    1


Sticking all together

.. code:: bash

    for n in $(seq 3 -1 1); do ./bottles.x $n; done

::

    3 green bottles hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 2 green bottles hanging on the wall
    2 green bottles hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 1 green bottles hanging on the wall
    1 green bottle hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 0 green bottles hanging on the wall

Gym with bash commands
~~~~~~~~~~~~~~~~~~~~~~

Keeping in mind the following commands

- ``ls``, ``mkdir``, ``cp``, ``mv``, ``rm``

make sure your are able to

- List the contents of the current directory

- List the contents of a directory

- Create a directory

- Create a directory tree

- Create a file in the directory tree

- Copy a file from a directory to the current one

- Remove a file

- Remove a directory (empty and not empty)

- Remove a directory tree

Batch scripting
---------------

Tasks automation
~~~~~~~~~~~~~~~~

Bash is perfect for automating small, repetitive tasks. All you have to do is writing down your commands in a simple file text (or **script**) and execute from the terminal. Contrary to the Fortran source code, scripts are not compiled: in scripting languages, commands are **interpreted** and executed at **run time**, one at a time.

Here we will revisit the idea of compiling the program ``bottles.f90`` and executing it with a range of input arguments. At the end, you will also see how to handle the output of bash commands and scripts and to move files around.

Let's start by collecting the commands we typed before in a file. We also add some comments for clarity with the ``#`` special character.

.. code:: bash

    # Compile the source code
    gfortran -o bottles.x bottles.f90

    # Execute the program
    for n in $(seq 3 -1 1); do
        ./bottles.x $n
    done

.. note::

    Did you notice the slight difference in layout? We keep the keyword ``do`` on the same line as ``for``: this way, the loop syntax looks pretty much like the one of other languages.

.. note::

    Within any programming language, special characters have a different meaning than their literal one. There is always a special character, however, to **escape** special character! In bash this is accomplished with the backslash ``\``. Try typing ``echo \#.``

.. note::

    Remember to **document your scripts** (and codes) with comments! You should at least (i) explain what the purpose of the script is and (ii) describe synthetically the main parts of the script.

You can also download the file directly at `this link <https://framagit.org/coslo/sinfo/-/raw/master/bash/src/script_0.f90>`_ (with `wget <https://manpages.org/wget>`_, of course!).

Executing a script
~~~~~~~~~~~~~~~~~~

We can execute the script this way

.. code:: bash

    bash script.sh

::

    3 green bottles hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 2 green bottles hanging on the wall
    2 green bottles hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 1 green bottles hanging on the wall
    1 green bottle hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 0 green bottles hanging on the wall

Alternatively, we can make the script **executable** and instruct the terminal that the commands have to be executed using ``bash``

1. Make the file executable by the user

.. code:: bash

    chmod u+x script.sh

1. Edit the file ``script.sh`` and add the following **she-bang** (a special symbols' combination) at the very beginning of the file

.. code:: bash

    #!/bin/bash

Note the exclamation mark!

Now you can execute the script like this

.. code:: bash

    ./script.sh

::

    3 green bottles hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 2 green bottles hanging on the wall
    2 green bottles hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 1 green bottles hanging on the wall
    1 green bottle hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 0 green bottles hanging on the wall

Make the script more flexible and robust
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

What if we want to compile and execute a different file? We could change the name of the file in the script, but we'd have to do it everytime.

We can make our script more flexible by specifying the path of the file to compile on the command line. The variable ``$1`` holds the first **positional argument** passed to the script. Let's edit the script to make it look like this:

.. code:: bash

    path=$1
    gfortran -o bottles.x $path

This will overwrite the ``bottles.x`` executable. We can do something a bit fancier: name the executable with the same **basename** as the source file but a different suffix. We use the `basename <https://manpages.org/basename>`_ command to strip the ``f90`` suffix

.. code:: bash

    basename bottles.f90 .f90

::

    bottles


We can add the new suffix by running ``basename`` in a **subshell** with the ``$()`` syntax and get the basename as output

.. code:: bash

    echo $(basename bottles.f90 .f90).x

::

    bottles.x


.. warning::

    If present, ``basename`` also strips the parent *directory* of the file from the path. A more robust approach is to complement ``basename`` with `dirname <https://manpages.org/dirname>`_, which removes the basename from the path instead.

Sticking all together in our script

.. code:: bash

    # Compile the source code
    path=$1
    bin=$(basename $path .f90).x
    gfortran -o $bin $path

    # Execute the program
    for n in $(seq 3 -1 1); do
        ./$bin $n
    done

And we execute it

.. code:: bash

    bash script.sh bottles.f90

Our script does not behave well when no argument is given on the command line, because

1. the compilation fails because ``$path`` is empty

2. the loop is executed despite the fact that the compilation failed

To fix these details, we will use the most obvious **control flow** statement: `if <https://www.gnu.org/software/bash/manual/bash.html#Conditional-Constructs>`_.

First, let's make sure a command line argument is given. We will use an intrinsic `test <https://www.gnu.org/software/bash/manual/bash.html#index-test>`_ `syntax <https://www.gnu.org/software/bash/manual/bash.html#Bash-Conditional-Expressions>`_ in bash to check whether a variable is empty

.. code:: bash

    if [ -z $path ] ; then
        echo path is empty
    else
        echo path is not empty: $path    
    fi

We will now stop the script if ``$path`` is empty and write an error message

.. code:: bash
    :name: usage

    if [ -z $path ] ; then
        echo Usage: $(basename $0) <path>
        echo Please provide the <path> to an source file
        exit 1
    fi

.. hint::

    The number following the ``exit`` command is an return code. By convention, a successful command or script returns 0.

.. hint::

    Can you guess what the variable ``$0`` contains?

Let's fix the second issue now. If the compilation fails, we should not execute the loop but exit right away. The special variable ``$?`` hold the error code of the previous command.

.. code:: bash
    :name: body

    # Compile the source code
    bin=$(basename $path .f90).x
    gfortran -o $bin $path

    # Execute the program
    if [ $? == 0 ] ; then
        for n in $(seq 10 -1 1); do
    	./$bin $n
        done
    fi

Our script is now more robust!

.. code:: bash

    # Compile a fortran source code and execute the program
    # with a range of different input arguments
    if [ -z $path ] ; then
        echo Usage: $(basename $0) <path>
        echo Please provide the <path> to an source file
        exit 1
    fi

    # Compile the source code
    bin=$(basename $path .f90).x
    gfortran -o $bin $path

    # Execute the program
    if [ $? == 0 ] ; then
        for n in $(seq 10 -1 1); do
    	./$bin $n
        done
    fi

Storing the output of scripts in files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Finally, let's see how to store the output of the script to a file, directly from the command line. ``bash`` has a nice, compact syntax to redirect the output of a command to a file:

.. code:: bash

    echo "hello world!" > output.txt

Show the content of the file with `cat <https://manpages.org/cat>`_

.. code:: bash

    cat output.txt

::

    hello world!


If we type the command again, the file ``output.txt`` will be overwritten. To append to it instead, use ``>>``

.. code:: bash

    echo "hello world again!" >> output.txt
    cat output.txt

::


    hello world!
    hello world again!


We can now store our little song to a file

.. code:: bash

    bash script.sh bottles.f90 > song.txt
    cat song.txt

::


    3 green bottles hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 2 green bottles hanging on the wall
    2 green bottles hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 1 green bottles hanging on the wall
    1 green bottle hanging on the wall
    but if one green bottle should accidentally fall
    there'll be 0 green bottles hanging on the wall

Finally, files can be moved (or renamed) with the `mv <https://manpages.org/mv>`_ command

.. code:: bash

    mv song.txt green_bottles_song.txt

Check all the files with ``txt`` suffix using the **wildcard** syntax

.. code:: bash

    ls *.txt

::

    green_bottles_song.txt  output.txt


The ``song.txt`` is not there anymore, it has been moved to ``green_bottles_song.txt``.

Now clean up everything

.. code:: bash

    rm -f *.txt

.. code:: bash

    cd ../../../
    rm -rf strumenti_informatici/

Small project proposals
~~~~~~~~~~~~~~~~~~~~~~~

1) **Automated backup script**

   - basic version with full snapshots

   - incremental backups

   - automate backups with cronjobs

2) **Mass-handling of files**

   - renaming image files (ex. by modification date)

   - batch editing files (ex. fill in a template)

   - synchronizing directories over the network with ``rsync``
