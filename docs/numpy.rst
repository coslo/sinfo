==============================
NumPy for scientific computing
==============================




What we'll cover today
----------------------

- Basics of **NumPy** for scientific computing

- When are operations on numpy arrays **efficient** and when not?

- Super-easy formatted **I/O** with **numpy**

Introduction to numpy
---------------------

The ``numpy`` package provides general n-dimensional arrays to do efficient linear algebra on matrices of arbitrary dimensions. It is the building block of most of the scientific computing done with Python nowadays.

Constructing an array
~~~~~~~~~~~~~~~~~~~~~

Equivalent ways to create a 3d array, filled with zeros

.. code:: python

    import numpy as np

    # From a list (or iterable)
    np.array([0.0, 0.0, 0.0])

    # Using the numpy.zeros function
    np.zeros(3)

The ``dtype`` argument gives us control on the `datatype and precision <https://numpy.org/doc/stable/reference/arrays.dtypes.html>`_. These are equivalent ways to get an array of floating point numbers in double precision

.. code:: python

    np.zeros(3)
    np.zeros(3, dtype=float)
    np.zeros(3, dtype=np.float)
    np.zeros(3, dtype=np.float64)
    np.zeros(3, dtype='d')
    np.zeros(3, dtype=np.dtype('d'))

Arrays have some useful attributes, among them let me mention

- ``shape``: a tuple describing the size of the array along each of its dimensions

- ``dtype``: the datatype of the array

Check the size and datatype of our position array

.. code:: python

    pos = np.zeros(3)
    print(pos.shape, pos.dtype)

::

    (3,) float64


Create an array of integer with single precision (32 bits, compatible with Fortran default integer precision) and inspect the largest possible value

.. code:: python

    ids = np.array([1, 2, 3], dtype=np.int32)
    print(np.iinfo(ids.dtype))

::

    Machine parameters for int32
    ---------------------------------------------------------------
    min = -2147483648
    max = 2147483647
    ---------------------------------------------------------------


We can create `empty arrays <https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html>`_ of any shape and type, to do linear algebra on matrices of arbitrary dimensions

.. code:: python

    # Number of particles
    N, ndim = 10,  3
    pos = np.ndarray((N, ndim))

See the `documentation <https://numpy.org/doc/stable/reference/generated/numpy.array.html>`_ for more details about creating numpy arrays.

Accessing and modifying array elements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To access and modify array elements, we use the same **subscript syntax** as for lists and tuples, like ``pos[0]``. Dealing with multi-dimensional arrays requires some further explanation

.. code:: python

    pos = np.ndarray((3, 3))

    # Access the third element of the first entry of pos
    # Equivalent ways
    print(pos[0][2])
    print(pos[0, 2])

::

    0.0
    0.0


By default, Python uses **C order** for storing multidimensional arrays in memory: arrays are stored by **row** so that elements are contiguous in memory along the "last" axis (`row major order <https://en.wikipedia.org/wiki/Row-_and_column-major_order>`_).

.. image:: ./order.png

This is important when interfacing code to Fortran (see later). To provide a smooth passage of arrays between Python and Fortran, we can arrange the arrays in **Fortran order** instead (`column major order <https://en.wikipedia.org/wiki/Row-_and_column-major_order>`_)

.. code:: python

    # order can be 'C' or 'F'
    pos = np.ndarray((3, 3), order='F')
    print(pos.flags['F_CONTIGUOUS'])

    # We can transform an array in F order
    pos = np.asfortranarray(np.ndarray((3, 3)))
    print(pos.flags['F_CONTIGUOUS'])

::

    True
    True


Numpy arrays have powerful Fortran-like **vector syntax** to operate on arrays. We showcase it by reimplementing the calculation of the center of mass of the system. We assign the following coordinates to the particles:

- :math:`\vec{r}_0 = (0, 0, 0)`

- :math:`\vec{r}_1 = (1, 1, 1)`

- :math:`\vec{r}_2 = (-1, -1, 1)`

so that the CM is at :math:`(0, 0, 0)`

.. code:: python

    N, ndim = 3, 3
    mass = np.ones(N)
    pos = np.zeros((N, ndim))

    # Adjust particle positions
    pos[1, :] = 1.0
    pos[2, :] = -1.0

    # Compute CM
    cm = np.ndarray(ndim)
    cm[:] = 0.0  # note the [:] here
    for i in range(pos.shape[0]):
        cm += pos[i, :] * mass[i]
    cm /= sum(mass)

    print(cm)

::

    [0. 0. 0.]


.. warning::

    Initializing the CM position as ``cm = 0.0`` will give the same final result, but it is not quite correct. Make sure you understand the difference wrt ``cm[:]=0.0``!

It is possible to operate on array subsections using a powerful **slicing** syntax

.. code:: python

    # Print the positions of the first two particles
    print(pos[0: 2, :])

    # Copy the first position into the last one
    pos[-1, :] = pos[0, :]

::

    [[0. 0. 0.]
     [1. 1. 1.]]


The "vector syntax" expresses the fact that some assignments can be performed **element-wise**. Fortran programmers will recognize the familiar syntax for vector operation, which, indeed, was introduced in Fortran years before ``numpy`` was born! Follow the `Rosetta stone <https://www.fortran90.org/src/rosetta.html>`_ for details about matching NumPy and Fortran syntax.

Zero-dimensional arrays
~~~~~~~~~~~~~~~~~~~~~~~

Numpy also provides **0-dimensional arrays** as scalars. Contrary to regular float and int types in Python, 0-dimensional arrays are *mutable*. We declare the Planck constant as a 0-dimensional array

.. code:: python

    h = np.array(6.62607015)  # in units of J/s
    print(h, h.shape, type(h), id(h))

::

    6.62607015 () <class 'numpy.ndarray'> 140317179038192


.. note::

    The shape of a 0-array is an empty tuple.

We scale :math:`h` **in-place** using the ``/=`` syntax

.. code:: python

    h /= (2*np.pi)
    print(h, type(h), id(h))

::

    1.0545718176461565 <class 'numpy.ndarray'> 140317179038192


There is no re-referencing here: we have modified the state of the object. This can be useful later on when interfacing Python with Fortran code.

Mutability of arrays
~~~~~~~~~~~~~~~~~~~~

Numpy arrays are mutable and subscriptable: it is crucial, however, to use the ``[...]`` syntax for in-place modifications

.. code:: python

    x = np.array([0, 1, 2])
    x[0] = 1  # subscriptable
    print(x, id(x))
    x[:] = x + 1  # in-place modification
    print(x, id(x))
    x += 1  # in-place modification
    print(x, id(x))
    x = x + 1  # reassignment!
    print(x, id(x))

::

    [1 1 2] 140110664534320
    [2 2 3] 140110664534320
    [3 3 4] 140110664534320
    [4 4 5] 140110664534608


For 0-dimensional arrays, mutability may lead to non-local effects not occurring with regular float's and int's

.. code:: python

    x = np.array(1)
    y = x
    x += 1
    print(x, y)

::

    2 2


Of course, that's because the modification is done in-place and both ``x`` and ``y`` refer to the same object.

To check if two arrays share *some* data, use

.. code:: python

    print(np.shares_memory(x, y))

::

    True

Numpy functions and array methods
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Numpy arrays have a number of useful `methods <https://numpy.org/doc/stable/reference/arrays.ndarray.html#array-methods>`_ and built-in functions to `manipulate them <https://numpy.org/doc/stable/reference/routines.array-manipulation.html>`_. We will only cover two examples, which will be useful to rewrite the nano-particle setup using numpy arrays.

Summing array elements
^^^^^^^^^^^^^^^^^^^^^^

The sum of the elements of a list can be done with the ``sum`` intrinsic function. This also works with numpy arrays, because they are iterable

.. code:: python

    print('Total mass:', sum(mass))

::

    Total mass: 3.0


There are intrinsic numpy functions and methods to do the same operation, possibly in a more efficient way (see below!)

.. code:: python

    np.sum(mass)  # function
    mass.sum()  # method

Adding elements to an array
^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is possible to `add and remove elements of numpy arrays <https://numpy.org/doc/stable/reference/routines.array-manipulation.html#adding-and-removing-elements>`_ in a similar way as with lists. However, this cannot be done in-place

.. code:: python

    mass.append(1)

::

    AttributeError: 'numpy.ndarray' object has no attribute 'append'


We must instead create a new array with the ``append`` function

.. code:: python

    mass = np.ones(3)
    mass_new = np.append(mass, [1.0])
    print('It should be 3+1:', mass_new.shape[0])

    pos = np.zeros((2, 3))
    pos_new = np.append(pos, [[1.0, 1.0, 1.0]], axis=0)
    print('It should be (2+1, 3):', pos_new.shape)

::

    It should be 3+1: 4
    It should be (2+1, 3): (3, 3)


Notice that the ``append`` function returns a **new array**. When arrays are large, appending creating new arrays with ``append`` may be time consuming, and it may be better to first create a list and then convert it to an array with ``array()``.

I/O with numpy
~~~~~~~~~~~~~~

Finally, ``numpy`` provides high-level functions to **read/write data** from/to files at with minimal effort.

To write the positions to file

.. code:: python

    def lattice(N):
        return np.ones((N, 3))
    pos = lattice(10)
    np.savetxt('data.txt', pos)

Check out the first 5 lines of the file

.. code:: sh

    head -n 5 data.txt

::

    1.000000000000000000e+00 1.000000000000000000e+00 1.000000000000000000e+00
    1.000000000000000000e+00 1.000000000000000000e+00 1.000000000000000000e+00
    1.000000000000000000e+00 1.000000000000000000e+00 1.000000000000000000e+00
    1.000000000000000000e+00 1.000000000000000000e+00 1.000000000000000000e+00
    1.000000000000000000e+00 1.000000000000000000e+00 1.000000000000000000e+00


The ``savetxt()`` function provides various arguments to control formatting.

Reading the file back can be done easily with the ``loadtxt()``

.. code:: python

    pos = np.loadtxt('data.txt')
    # Check the data
    print(pos[:5, :])

::

    [[1. 1. 1.]
     [1. 1. 1.]
     [1. 1. 1.]
     [1. 1. 1.]
     [1. 1. 1.]]


Use the ``unpack=True`` argument to get back the data as columns.

There are various ways to control the parsing of variables via ``loadtxt()``. Check out the `documentation <https://numpy.org/doc/stable/reference/generated/numpy.loadtxt.html>`_.

Efficiency
----------

First of all, a warning message:

::

    We should forget about small efficiencies, say about 97% of the time:
    premature optimization is the root of all evil

    - Donald Knuth

.. image:: ./knuth.png

In general, three levels:

1. Algorithms

2. Optimization

3. Parallelism

The key to efficient computing with numpy is **operating on entire arrays, or array slices, at once**. The reason is that accessing individual array elements incurs in a significant **overhead**. It is crucial that the arrays or array slices are **large enough** to overcome the overhead of operating on them!

We consider here a few simple examples, revisiting the calculation of the nano-particle CM. We can **measure the time** spent by the code with the ``time`` module, adding these lines of code at the beginning and at the end of each block.

.. code:: python
    :name: time_start

    import time
    ti = time.time()

.. code:: python
    :name: time_stop

    tf = time.time()
    print('Elapsed time: {:.2f} s'.format(tf-ti))

These lines of codes will be stripped from this page for clarity. In a Jupyter notebook, add the ``%%time`` magic line at the beginning of each code cell you want to time. Some IDEs provide timing info of code blocks out of the box.

Reduction operations
~~~~~~~~~~~~~~~~~~~~

We reconsider the calculation of the total mass of the system. We repeat the operation multiple time to increase the computation time (not need with ``%%timeit`` magic)

.. note::

    Operations (sum, product, etc) that output a scalar from an array are called "reduction" operations.

Small arrays
^^^^^^^^^^^^

We start with the small array we created above (:math:`N=3`).

1) We iterate over the individual array elements

.. code:: python

    for _ in range(1000000):
        tot = 0.0
        for i in range(len(mass)):
            tot += mass[i]

::

    Elapsed time: 0.82 s


1) Using the ``sum`` Python intrinsic function takes more time

.. code:: python

    for _ in range(1000000):
        sum(mass)

::

    Elapsed time: 0.96 s


1) Use the ``numpy.sum`` function

.. code:: python

    for _ in range(1000000):
        np.sum(mass)

::

    Elapsed time: 2.87 s


Ops, the numpy function is slower! That's clear: the mass array is *too small* and the overhead of calling ``sum`` overwhelms the actual computing time.

Large arrays
^^^^^^^^^^^^

Let's take a larger system, :math:`N=10000`, and repeat the analysis

.. code:: python

    mass = np.ones(10000)

1) We iterate over the individual array elements

.. code:: python

    for _ in range(1000):
        tot = 0.0
        for i in range(len(mass)):
            tot += mass[i]

::

    Elapsed time: 1.86 s


1) Using the ``sum`` Python intrinsic function now is faster

.. code:: python

    for _ in range(1000):
        sum(mass)

::

    Elapsed time: 0.70 s


1) Use the ``numpy.sum`` function...

.. code:: python

    for _ in range(1000):
        np.sum(mass)

::

    Elapsed time: 0.01 s


BOOM! With *large enough* arrays, the ``numpy.sum()`` function becomes a few orders of magnitude faster!

Vectorized operations
~~~~~~~~~~~~~~~~~~~~~

Lists are very powerful and flexible data structures, but they are too slow for number crunching calculations: accessing the entries of a list is pretty inefficient. The same holds for numpy arrays, **unless we operate on large arrays or array sections**. This can be done by using the "vector-syntax" familiar to Fortran programmers.

.. note::

    In the numpy jargon, operations carried out on array sections are said to be "vectorized" (not to be confused with `SIMD vectorization <https://en.wikipedia.org/wiki/Single_instruction,_multiple_data>`_).

We revisit the center of mass calculation, using a large system this time

.. code:: python

    N, ndim = 10000, 3
    mass = np.ones(N)
    pos = np.zeros((N, ndim))

This this is original version of our code

.. code:: python

    # Repeat the inner loop many times and time the execution
    for _ in range(100):
        # Compute CM
        cm = np.zeros(ndim)
        for i in range(pos.shape[0]):
            cm += pos[i, :] * mass[i]
        cm /= sum(mass)

::

    Elapsed time: 1.60 s


What if we had done the inner sum iterating over individual array elements?

.. code:: python

    # Repeat the inner loop many times and time the execution
    for _ in range(100):
        # Compute CM
        cm = np.zeros(ndim)
        for i in range(pos.shape[0]):
            # Explicit loop
            for j in range(pos.shape[1]):
                cm[j] += pos[i, j] * mass[i]
        cm /= sum(mass)

::

    Elapsed time: 1.85 s


It is not much slower, just a little bit. That's because the slice ``pos[i, :]`` is small, hence the overhead is still significant. 

To optimize the code using plain ``numpy,`` we must sum the individual coordinates operating on larger array portions. We need the product of the :math:`j` cartesian coordinate times the mass.

.. code:: python

    # Repeat the inner loop many times and time the execution
    for _ in range(100):
        # Compute CM
        cm = np.zeros(ndim)
        for j in range(pos.shape[1]):
            cm[j] = np.sum(pos[:, j] * mass[:])
        cm /= sum(mass)

::

    Elapsed time: 0.08 s


That's much better now! The operation ``pos[:, j] * mass[i]`` is "vectorized" over a large array portion and the sum incurs in little overhead (only three calls).

.. warning::

    This time the modification to make our code efficient was pretty simple. However, achieving numpy vectorization may require rewriting our algorithm and sometimes this makes it less readable / less natural.

Visualization
-------------

To visualize the nanoparticle you can try with `the matplotlib scatter function <https://matplotlib.org/stable/gallery/mplot3d/scatter3d.html>`_, but it won't be very nice. An alternative visualization package, which works well in a jupyter notebook, is `py3dmol <https://pypi.org/project/py3Dmol/>`_.

To install py3dmol in your virtual environment

.. code:: sh

    pip install py3dmol

You can now visualize configurations from a jupyter notebook with the following code. The input ``positions`` must be provided as a list of lists or as a 2-dim array with layout ``(N, 3)``, where ``N`` is the number of particles.

.. code:: python

    def visualize(positions, colors=None, radii=None, color='white', radius=0.5):
        """
        Visualize a particle configuration using 3dmol http://3dmol.csb.pitt.edu/
        """
        import py3Dmol
        if colors is None:
            colors = [color] * len(positions)
        if radii is None:
            radii = [radius] * len(positions)
        view = py3Dmol.view()
        view.setBackgroundColor('white')
        for i in range(len(positions)):
            view.addSphere({'center': {'x': positions[i][0],
                                       'y': positions[i][1],
                                       'z': positions[i][2]},
                            'radius': radii[i], 'color': colors[i]})
        return view

    visualize([[0, 0, 0], [1, 0, 0]])

If the configuration is not in ``(N,3)`` layout, just transpose the array before passing it to the function.

Exercise
--------

You are now ready to use **numpy arrays** to code the **nano-particle** setup! No need to worry much about efficiency at this stage. Just code it using the features of numpy arrays we have seen above. Encapsulate the code in functions to make them reusable, one for the setup of the lattice

.. code:: python

    def lattice(M, a=1):
        """
        Return a numpy array with particles' position on a lattice of
        spacing `a`, with `M` cells per side
        """
        # your code here

another function to compute the center of mass

.. code:: python

    def center_or_mass(position, mass):
        """
        Return the center of mass of a system of particles 
        - at positions `position` [(N, ndim)-dimensional array]
        - with masses `mass` [N-dimensional array]    
        """
        # your code here

and finally a function to produce the nano-particle (which will internally call ``lattice()`` and ``center_of_mass()``)

.. code:: python

    def nano_particle(radius=3.0):
        """
        Return the positions of a nanoparticle of given `radius`.
        """
        # your code here

Visualize the nanoparticle with the ``visualize`` function above to make sure it looks as expected!

Note
----

.. code:: fortran

    program main
      real(8) :: x(3)
      x = 1.0
    end program
