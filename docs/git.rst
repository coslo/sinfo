================================
Git, github, gitlab and all that
================================




Version control with git
------------------------

Useful links
~~~~~~~~~~~~


- The *official* git documentation: `https://git-scm.com/doc <https://git-scm.com/doc>`_

- Beginner tutorial by *gitlab*: `https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html <https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html>`_

- Beginner tutorial by *github*: `https://docs.github.com/en/github/getting-started-with-github/quickstart <https://docs.github.com/en/github/getting-started-with-github/quickstart>`_

- Questions on *stack overflow*: `https://stackoverflow.com/questions/tagged/git <https://stackoverflow.com/questions/tagged/git>`_

Git in a nutshell
~~~~~~~~~~~~~~~~~

.. image:: ./Git-logo.png

Git is a **version control system** (VCS)

- Keep **history** of modifications to files

- Single-user or team **software development**

- Created by Linus Torvalds in 2005 to support the linux kernel development

- As of today, the **most popular** VCS out there

It is a *command line tool*. Check if you have it:

.. code:: sh

    git --version

::


    git version 2.34.1


**Found it?** Cool, then read on! If not: `https://git-scm.com/downloads <https://git-scm.com/downloads>`_

What do *I* use it for?
~~~~~~~~~~~~~~~~~~~~~~~

Over the past 10+ years:

- **code development**

- **configuration files** (my ``.bashrc``, ``.emacs``, etc.)

- **research papers** - with *geek* colleagues only :-(

- website, curriculum, tutorials, notebooks, ...

All this can be done

- as a **single user** or in **collaboration** with others

- both **offline** on a machine (ex. my laptop) or...

- ... over a **network** (I currently use `https://framagit.org/ <https://framagit.org/>`_ as main server)

What will *you* use it for, today?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- **Keep a text document** (code, report, ...) under version control

- Fine tune **git configuration**

- Edit a text document **collaboratively**

Quick start
~~~~~~~~~~~

To keep track of the modification history of the files in a folder, turn that folder into a git **repository**.

Increments in the modification history appear as ``commits``: each commit stores a set of modifications to the project, like **adding**, **modifying** or **removing** files.

Create a new folder, say ``project/,`` cd into it and initialize the repository

.. code:: sh

    mkdir project
    cd project
    git init

::


    hint: Using 'master' as the name for the initial branch. This default branch name
    hint: is subject to change. To configure the initial branch name to use in all
    hint: of your new repositories, which will suppress this warning, call:
    hint:
    hint: 
    hint: Names commonly chosen instead of 'master' are 'main', 'trunk' and
    hint: 'development'. The just-created branch can be renamed via this command:
    hint:
    Initialized empty Git repository in /home/coslo/teaching/sinfo/git/project/.git/

.. note::

    The history of your repository, along with its configuration, are stored in the ``.git/`` folder. If you delete that folder, the git repository is gone!

Add a README file in the ``project`` folder

.. code:: sh

    echo "Hello World!" > README.md
    git status

::


    On branch master

    No commits yet

    Untracked files:
    ..." to include in what will be committed)
    	README.md

    nothing added to commit but untracked files present (use "git add" to track)

Now add the file to the project and commit the change

.. code:: sh

    git add .  # the dot will add all new files in the folder
    git commit -m "Initial commit"

::


    [master (root-commit) e437035] Initial commit
     1 file changed, 1 insertion(+)
     create mode 100644 README.md

More on commits
~~~~~~~~~~~~~~~

As you can see, this is done in two steps:

- ``add``: include the new file into the list of modifications to record

- ``commit``: actually store the modifications in the repository

The ``-m`` flag allows you to leave a "commit message" to describe the change.

Modify the file and check the difference

.. code:: sh

    echo "Hello World, again!" >> README.md
    git diff

::

    diff --git a/README.md b/README.md
    index 980a0d5..5eb476f 100644
    --- a/README.md
    +++ b/README.md
    @@ -1 +1,2 @@
     Hello World!
    +Hello World, again!


Commit the change

.. code:: sh

    git commit -am "Update README"

::

    [master ccd8fd8] Update README
     1 file changed, 1 insertion(+)


If we modify some files, we can "add" the change and commit in one shot with the ``-a`` flag.

Have a look at what we have done so far

.. code:: sh

    git log

::

    commit ccd8fd808ef882564819f26178a2d46b78871600 (HEAD -> master)
    Date:   Mon Oct 9 18:14:43 2023 +0200

        Update README

    commit e437035bbf22baa2c08270be7da2df4cefc26b6f
    Date:   Mon Oct 9 18:14:16 2023 +0200

        Initial commit

Finally, removing a file is similar to adding it

.. code:: sh

    touch useless_file.txt
    git add useless_file.txt
    git commit -m "Add useless file"

    git rm useless_file.txt
    git commit -m "Remove useless file"

::


    [master 11c358d] Add useless file
     1 file changed, 0 insertions(+), 0 deletions(-)
     create mode 100644 useless_file.txt
    rm 'useless_file.txt'
    [master 34e35c8] Remove useless file
     1 file changed, 0 insertions(+), 0 deletions(-)
     delete mode 100644 useless_file.txt


**Moral #1**: Useful commands to check the status of your repository are

- ``git status``

- ``git diff``

- ``git log``

**Moral #2**: Basic commands to add / remove files and commit changes

- ``git add``

- ``git rm``

- ``git commit``

Fine-tuning
~~~~~~~~~~~

Personal information
^^^^^^^^^^^^^^^^^^^^

This is not necessary, but you may want to specify your name and e-mail (actually, git will prompt you to do that on the first commit)

.. code:: bash

    git config user.name <your_name>
    git config user.email <your_email>

Add the ``--global`` flag to apply these settings globally for all your repositories

.. warning::

    *Privacy notes*:

    - If you plan to share your project publicly, consider using a dummy (inexistent) email account

    - git commits may reveal a lot of information about a user's habits...

Existing files
^^^^^^^^^^^^^^

What if the folder *already* contains files and/or sub-folders and we want to some (but not all) of them to the repository?

*Option 1*: add all the files then remove the ones you do not need

.. code:: bash

    git add .
    git rm <not_needed_file>
    git commit -m "Add files"

*Option 2*: add selected files

.. code:: bash

    git add <needed_file_1> <needed_file_2>
    git commit -m "Add files"

You can use the star syntax (ex. ``*.tex``) to match multiple file

Ignoring files
^^^^^^^^^^^^^^

You may want to avoid storing certain files in your git repository, for instance

- temporary or backup files (``*.~``, ``*.bak``)

- compilation artifacts and executables (``*.o``, ``*.so``, ``*.x``)

Create a text file called ``.gitignore`` at the root of your repositoriy and fill it with patterns matching the files to ignore

.. code:: sh

    cat > .gitignore <<EOF
    *~
    *.o
    EOF

    git add .gitignore
    git commit -am "Add gitignore"

::


    [master c5decd3] Add gitignore
     1 file changed, 2 insertions(+)
     create mode 100644 .gitignore


From now on, ``git status`` will ignore these files and they will not be considered for commit

FAQ
~~~

How do I go back in time?
^^^^^^^^^^^^^^^^^^^^^^^^^

First look for the *hash* of the commit you are interested in

.. code:: sh

    git log --pretty=oneline

::

    c5decd31ecc1dcfafe280d6587bacda7feacdb8d (HEAD -> master) Add gitignore
    34e35c885b157e3160c60ba441a233e6c188fca1 Remove useless file
    11c358de502f9056402d27ac169604876b14146f Add useless file
    ccd8fd808ef882564819f26178a2d46b78871600 Update README
    e437035bbf22baa2c08270be7da2df4cefc26b6f Initial commit


Check out a copy of the project as it was in that commit

.. code:: bash

    git checkout c5decd

To get back to the current state of the project

.. code:: bash

    git checkout master

How can I undo the last commit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is one of the `most popular question <https://stackoverflow.com/questions/927358/how-do-i-undo-the-most-recent-local-commits-in-git>`_ about git on stackoverflow.

.. code:: sh

    git commit -m "Something terribly misguided"
    git reset HEAD~

Then edit the files as necessary and commit again.

Do I *really* need to write those commit messages?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Writing commit messages may seem a burden (and actually, you can leave them blank) but it *is* important.

.. hint::

    You may be able to write commit messages directly from your IDE and/or text editor.

.. image:: ./git_commit_2x.png

There are well-established guidelines to write **good** commit messages, so that they are informative and easy to read
`https://cbea.ms/posts/git-commit/ <https://cbea.ms/posts/git-commit/>`_

**Structure**

- *Separate subject from body with a blank line*

- Use the body to explain what and why vs. how

**Style**

- *Use the imperative mood in the subject line*

- Capitalize the subject line

- Do not end the subject line with a period

- Limit the subject line to 50 characters

- Wrap the body at 72 characters

.. hint::

    Write "Fix typo in tutorial", not "fixed typo in tutorial."

Software versioning
~~~~~~~~~~~~~~~~~~~

For basic usage, *explicit* versioning is not necessary: use git hashes to identify commits. However, it is *crucial* for libraries and API, or any code on which *other* codes may depend upon.

- ``X``: simple incremental versioning

- ``X.Y``: major-minor (ex: ``0.1``, that's where you start from)

- ``X.Y.Z``: major-minor-patch (ex. git ``2.17.1``)

- ``X.Y.Z.*``: alpha / beta / release candidate info

One of the most sensible ways to increment versions is `semantic versioning <https://semver.org>`_.

In a nutshell:

- increase ``X``: **backward incompatible changes**

- increase ``Y``: new features

- increase ``Z``: *bug fixes, cosmetics, optimization*

Explicit versioning in git is handled via ``tags``. 

Tags are snapshots of the state of a project to which git assigns a meaningful identifier

.. code:: bash

    git commit -m "Bump version 0.1.0" 
    git tag -am "0.1.0" 0.1.0

List all tags

.. code:: bash

    git tag -l

::

    0.1.0


Check out the project at a specific tag

.. code:: sh

    git checkout 0.1.0

Use ``git checkout master`` to get back to the current state of the project.

Branches
~~~~~~~~

Branches allow you to keep **multiple versions of your project** in parallel.

Repositories are created with a default branch, typically called **master** or **main**. That's the branch to which we committed the modifications above.

Typical use case: develop a new feature in a separate *feature branch*, then merge it in the master branch.

.. image:: branch1.png

Glossary:

- *Fast-forward*: no commits between the creation of the new branch and the merge

- *Conflict*: concurrent modification to the same part of a file

Create a new branch and check it out

.. code:: sh

    git checkout -b new_cool_feature
    # commit commit commit ...

::

    Switched to a new branch 'new_cool_feature'


Show available branches

.. code:: sh

    git branch -va

::

     master            c5decd3 Add gitignore
    * new_cool_feature c5decd3 Add gitignore


The prompt of your terminal should tell you which branch you are working on

::

    coslo@local:~/teaching/sinfo/git/project/(new_cool_feature)$

If not, you may want to follow these instructions `https://blog.sellorm.com/2020/01/13/add-the-current-git-branch-to-your-bash-prompt/ <https://blog.sellorm.com/2020/01/13/add-the-current-git-branch-to-your-bash-prompt/>`_

When the new feature is ready, get back to the master branch and merge the feature branch

.. code:: sh

    git checkout master
    git merge new_cool_feature

::

    Switched to branch 'master'
    Already up to date.


If **conflicts** occur, you will have to modify the files to remove them. ``git status`` will tell you how to proceed.

.. warning::

    Beware! Conflict handling can be nasty...

Exercise: thesis in latex
~~~~~~~~~~~~~~~~~~~~~~~~~

- Make sure ``git`` is installed on your machine (if not, follow `https://git-scm.com/downloads <https://git-scm.com/downloads>`_)

- Download and extract the latex template from `this link <https://framagit.org/coslo/template-latex/>`_ (do not clone the repository)

- Turn it into a new git repository in a folder called ``thesis``: add all the relevant files and commit your changes

- Familiarize yourself with the commands seen above (except those related to branching, unless you are already confident with the git basics)

Collaborative editing
---------------------

Remote repositories
~~~~~~~~~~~~~~~~~~~

Git is a **distributed VCS**: several copies of the same repository may exist and exchange with one another.

Multiple instances of a project may exist:

- **locally** on a machine

- over a **network**, communicating through various protocols (ssh, git, https)

From the perspective of a given repository, other repositories of the same project are called **remotes**. Once remotes are set up, the workflow is identical whether the repository copies are on the same hard-disk or on a different machine.

Typically, one keeps a repository as an "official" source for the project. This official repository must be a ``bare`` one: there are no checked out files in it.

**Example**:

- all repositories are "local", i.e. on the same disk

- two repositories cloned from ``project``

- ``project_1`` is a remote of ``project_2``, named ``extra``

.. image:: dia0.png

Initialize a bare "official" repository in the ``project`` folder and clone two copies

.. code:: sh

    git init --bare project
    git clone project project_1
    git clone project project_2

::

    Initialized empty Git repository in /home/coslo/teaching/sinfo/git/project/
    Cloning into 'project_1'...
    warning: You appear to have cloned an empty repository.
    done.
    Cloning into 'project_2'...
    warning: You appear to have cloned an empty repository.
    done.

.. image:: dia0b.png


Add ``project_1`` as a remote of ``project_2``

.. code:: sh

    cd project_2
    git remote add extra ../project_1

The syntax is ``git remote add <remote_name> <remote_path>``

- ``<remote_name>`` is a name for the remote

- ``<remote_path>`` is the path to the remote (can be over the network, see below)

Have a look at the remote repositories we have access to

.. code:: sh

    git remote -v show

::

    extra	../project_1 (fetch)
    extra	../project_1 (push)
    origin	/home/coslo/teaching/sinfo/git/project (fetch)
    origin	/home/coslo/teaching/sinfo/git/project (push)


By default, the ``origin`` remote is the one we cloned the project from.

We can now check out any branch of the ``project_1`` remote repository, inspect or grab files from them, merge them into our own branches.

**Example**: the master branch of ``project_1`` contains some interesting new commits and we want to merge them in our code:

.. code:: bash

    git pull extra master
    git checkout extra/master
    # ... look at code here

    # Get back to our master branch and merge the contect of extra/master
    git checkout master
    git merge extra/master

    # To keep a flat commit history, instead of merge use
    git rebase extra/master

Remote collaboration
~~~~~~~~~~~~~~~~~~~~

To **collaborate remotely**, users must have access to a git server. A git server may boil down to a simple ssh server, to which you and your collaborators have access to, or using a proper git server (with ``git`` protocol).

.. image:: images/dia2.png

The power of git allows you to deal with multiple remote repositories on *different* servers. Say Alice and Bob both cloned a project from a server. They can push this project to their respective git servers (github and gitlab).

.. image:: dia3.png


Then, Alice adds Bob's repository, which is stored on github, as remote...

.. code:: bash

    # On Alice PC
    git remote add bob https://github.com:bob/project.git

...and viceversa!

.. code:: bash

    # On Bob PC
    git remote add alice https://gitlab.com:alice/project.git

.. image:: dia4.png

Pushing and pulling each others' project can then be done with the same commands seen above for local remotes

.. code:: bash

    git remote add new_remote git@<your_server>:<your_username>/<your_project>.git
    git remote -v  # display remotes to check
    git pull --rebase new_remote master  # fetch and rebase remote master into local master

Github, gitlab and all that
~~~~~~~~~~~~~~~~~~~~~~~~~~~

git is a **decentralized** VCS: no central server, every repository can share with any other.

However, git servers have undergone a coarsening process and a few big **centralized** servers ("silos") have emerged.

Along this process, **web applications** grew around git servers, adding functionalities that allow users to browse repositories and collaborate on software:

- **github**: centralized, closed source, owned by Microsoft (`https://github.com/explore <https://github.com/explore>`_)

- **gitlab**: centralized, open-source edition (`https://gitlab.com/explore/projects/trending <https://gitlab.com/explore/projects/trending>`_)

- self-hosted instances of gitlab: ex. **framagit** (`https://framagit.org/ <https://framagit.org/>`_)

- **gittea** (`https://gitea.io/en-us/ <https://gitea.io/en-us/>`_), **gogs** (`https://gogs.io/ <https://gogs.io/>`_): self-hosted

- **codeberg** (`https://codeberg.org/ <https://codeberg.org/>`_): based on **forgejo**, which is itself a fork of **gittea**

- ActivityPub for federation

- ...

**Features**:

- **issue tracking**: report bugs or ask for new features

- **pull requests**: facilitate merging of new features and bug fixes

- **continuous integration**: automatic testing and deployment of code on multiple platforms

- **web page hosting**: host static web pages (ex. documentation)

- **social networking**: collaborate and discover new projects

**Anti-features**:

- social networking (!)

- walled gardens

- over-engineered

Creating an account
~~~~~~~~~~~~~~~~~~~

Both gitlab and github have free plans for *private* and *public* repositories. Here I create an account on gitlab `https://gitlab.com/users/sign_up <https://gitlab.com/users/sign_up>`_ (no need to give your actual identity)

.. image:: ./gitlab1.png

.. image:: ./gitlab2.png

Setting up ssh keys
~~~~~~~~~~~~~~~~~~~

*If you do not have them yet*, create a pair of private-public ssh keys. This will encrypt your communications to the remote server using the ``git`` protocol

.. code:: sh

    ssh-keygen
    # Press Enter three times... (or add a passphrase for security)

Your public key is

.. code:: sh

    cat ~/.ssh/id_rsa.pub

::

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDnnpooSARIJ6HYdy/QWrtv0C1XANNKWUIDei3BxCXy9f62hzAQMtJuVAr/
    wu0WfNnygcP1w+D/LOScdrRrzw5hCHainVcVQustw8OtkyW19ITboMD5KC9P2pdSPtsdaUNF8ZK0+fDfYwFpRfeF0+qsebQs
    OWaLF03x+o69ZnJf1Gs6PGER2aETBVz9UUniveMf0CqHGmUK5l5Yj9Otzs4uAAYEyvGPt5amtRWMGufQIB5edrOqa/+Mu8gI
    xu6uHUtLsEs9AzDslM/McWDVoDAumnBgJsLGJpD3/0DsD4dToH6NtRsNLhy8NmRotsJwL+KSIrT9zpVfk67 coslo@local


Your private key is

.. code:: sh

    cat ~/.ssh/id_rsa

.. danger::

    Never ever share your private key!

In the *SSH Keys* settings on the web interface (top right corner, then *Preferences*), add a new key by pasting the content of ``~/.ssh/id_rsa.pub``

.. image:: ./addssh.png

.. note::

    It is also possible to have or grant access to a remote repository with **tokens** (both for `gitlab <https://docs.gitlab.com/ee/security/token_overview.html>`_ and `github <https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token>`_)

Creating a new project
~~~~~~~~~~~~~~~~~~~~~~

You can push your own ``thesis`` git project directly to the gitlab server.

The default remote is called ``origin``. Add the gitlab server as the new ``origin`` remote, then push the ``master`` branch over there

.. code:: bash

    git remote add origin git@<your_server>:<your_username>/thesis.git
    git push -u origin master

.. warning::

    Remember to change the dummy names ("your\_server", "your\_username") above with yours!

To push all existing branches (and tags, if you have them)

.. code:: bash

    git push -u origin --all
    git push -u origin --tags

Differences between pull, merge and rebase
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First, note that 

.. code:: bash

    git pull origin master

is equivalent to

.. code:: bash

    git fetch origin
    git merge origin/master

Unless the merges resolves in a *fast-forward*, ``git`` will keep track of the branching path in the commit history. To always flatten commit history, make a *rebase* instead

.. code:: bash

    git pull --rebase origin master

or equivalently

.. code:: bash

    git fetch origin
    git rebase origin/master

Exercise: create an account on a git platform
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ex2.png

- choose a web platform of your choice

- create an account on it

- create your ssh keys and add *the public one* in your account

- create a new private project

- push your ``thesis`` repository over there

- browse your repository online

- *commit, commit, commit...* then push the changes to the server again

To upload existing files from your computer, use the instructions below. Remember to replace dummy paths with relevant ones.

Push an existing Git repository
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is the most common use case

.. code:: bash

    cd existing_repo
    git remote rename origin old-origin
    git remote add origin git@<your_server>:<your_username>/thesis.git
    git push -u origin --all  # -u sets origin as upstream server of every branch 
    git push -u origin --tags  # if you have tags

Push an existing folder
^^^^^^^^^^^^^^^^^^^^^^^

If ``existing_folder`` contains a repository but you want to start a brand new one, just delete the ``.git`` folder at its root

.. code:: bash

    cd existing_folder
    git init --initial-branch=main
    git remote add origin git@<your_server>:<your_username>/thesis.git
    git add .
    git commit -m "Initial commit"
    git push -u origin main

Create a brand new repository
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This assumes you create an empty repository on the remote server

.. code:: bash

    git clone git@<your_server>:<your_username>/thesis.git
    cd thesis
    git switch -c main
    touch README.md
    git add README.md
    git commit -m "add README"
    git push -u origin main

Project: collaborative editing of a document
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./dia4.png

- who plays Alice? who plays Bob?

- clone the repository ``template-latex`` from framagit

- create a new project on your remote server (either gitlab or github)

- set the ``origin`` of the repository you cloned to your new project on the remote server

- edit the ``thesis.tex`` file and commit the change

- push the change to your remote server

- add a new remote pointing to your collaborator's project on his/her remote server

- pull his/her commit into your own master branch

**Bonus**:

- handle conflicts if both of you modify the same parts of ``thesis.tex``!
