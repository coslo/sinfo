===================================
Strumenti informatici per la fisica
===================================




Overview
--------

This is a series of short informal tutorials on

1. efficient usage of the **terminal** and **bash** scripting

2. introduction to **Python**, **Jupyter** and **matplotlib**

3. computing in Python with **numpy**

4. interfacing **Python\*** and **Fortran** with **f2py** (vs. **numba**)

5. revision control with **git** (+ **github**, **gitlab** & all that)

The focus is on free and open source (`FOSS <https://en.wikipedia.org/wiki/Free_and_open-source_software>`_) software tools, both `low- and high-level <https://en.wikipedia.org/wiki/High-_and_low-level>`_.

**Free** (as in "free speech") software can be inspected, modified and redistributed by anyone. It implies the code is **publicly** available. It does not imply it is free as in "free beer" - although it mostly is.

1) **High-level** tools provide **simple interfaces** to perform complex tasks. They are meant to be **easy to use** and **intuitive**, but they also tend to be **rigid**.

2) **Low-level** tools provide **fine control** on the building blocks needed to accomplish a task. They are **flexible** and **powerful**, but their **learning curve** is **steeper**.

.. image:: ./tools.png

.. toctree::
   :hidden:
   :maxdepth: 1
   :glob:
      
   bash
   python
   numpy
   faster_python
   intermediate
   git

Web platforms and several IT services operate over a **network** (nodes connected by edge). Networks can be

- **centralized**: a single, central authority server to which all nodes must connect

- **decentralized**: no central authority, multiple *federated* servers

- **distributed:** each node can exchange directly with any other

.. image:: ./networks.png

Decentralized and distributed networks tend to be more `resilient <https://en.wikipedia.org/wiki/High_availability#Resilience>`_.

.. image:: ./chart.png
