=====================================
Introduction to Python for physicists
=====================================




Getting started
---------------

Things you'll learn today
~~~~~~~~~~~~~~~~~~~~~~~~~

We'll cover:

- setup of an isolated **Python environment** with ``venv`` and ``pip``

- **development** and **project** environments for Python

- basic **features** of Python and its key **differences** wrt other languages

- plotting numerical data with **matplotlib**

Final goal
~~~~~~~~~~

The goal of this series of tutorials on Python is to write a code to compute compute efficiently the energy of a **Lennard-Jones nanoparticle**

.. image:: ./nanocluster.png

The nanoparticle is a cluster composed of particles interacting with the Lennard-Jones potential


.. math::

    u(r) = 4 \epsilon [(r/\sigma)^{12} - (r/\sigma)^{6}]


where :math:`\epsilon` and :math:`\sigma` are constants that provide the units of energy and distance, respectively. The nanoparticle configuration will be cropped from an fcc crystal.

That's our little challenge for the next three tutorials!

Useful links and docs
~~~~~~~~~~~~~~~~~~~~~

Python tutorials proliferates on the internet. There are, however, a few authoritative sources of information:

- The official `Python tutorial <https://docs.python.org/3/tutorial/index.html>`_

- The official `documentation <https://docs.python.org/3/library/>`_

The documentation is also accessible from the **python intepreter itself** and within any **IDE**, **notebook** etc.

In addition

- `Stack overflow <https://stackoverflow.com/questions/tagged/python>`_ provides a wealth of information on both general and very specific issues about the Python language

- `The hitchhiker's guide to python <https://docs.python-guide.org/>`_ by Kenneth Reisz is an excellent, general-purpose guide to Python

- `Real Python tutorials <https://realpython.com/>`_ are pretty good, too

- The `w3school tutorial <https://www.w3schools.com/python/>`_ is quite extensive and code snippets can be executed live

Prerequisites
~~~~~~~~~~~~~

- a Python distribution with ``venv`` module installed

An overview of Python
---------------------

What is Python?
~~~~~~~~~~~~~~~

From the `Python tutorial <https://docs.python.org/3/tutorial/index.html>`_:

::

    Python is an easy to learn, powerful programming language. It has
    efficient high-level data structures and a simple but effective
    approach to object-oriented programming. Python’s elegant syntax and
    dynamic typing, together with its interpreted nature, make it an ideal
    language for scripting and rapid application development in many areas
    on most platforms.

Beyond object-orientation, it also supports other **programming paradigms**, such as procedural and functional programming. It can be **interfaced with other languages** (via C or C++) - we'll do that with Fortran. Moreover, it is **free** software!

Perfect **use cases**:

- **scripting** beyond what bash can do

- **prototyping** code

- **interfacing** with compiled languages

- high-level **data analysis** and **visualization**

**Shortcomings**, especially for scientific computing:

- efficient coding can be tricky

- lack of backward compatibility

- its *dark side*

- ecosystem evolution is too rapid

The Python ecosystem
~~~~~~~~~~~~~~~~~~~~

Built upon:

- `Python standard library <https://docs.python.org/3/library/index.html>`_: specification of the core language features and modules

- CPython: an **official distribution** that implements the standard library (typically installed in ``/usr/lib/python3/dist-packages/)``

- Python distributions alternative to the official one (ex. `pypy <https://www.pypy.org/pypy>`_, `anaconda <https://www.anaconda.com/>`_)

- `Python Package Index <https://pypi.org/>`_ (**pypi**): a large, official repository of third-party modules and packages

- **Package managers**: software to install packages and handle their dependencies (ex. `pip <https://pip.pypa.io/en/stable/>`_, `poetry <https://python-poetry.org/>`_)

- **Virtual environments**: isolated, local installations of Python distributions (ex. `venv <https://realpython.com/python-virtual-environments-a-primer/>`_, `conda <https://docs.conda.io/en/latest/>`_)

Virtual environments
~~~~~~~~~~~~~~~~~~~~

We want to

- provide our project with a well-defined software stack

- isolate the software environment associated to a project from the rest of the OS

.. warning::

    Installing packages from your OS package manager does not guarantee reproducibility

**Solution**: use virtual environments

- Python **virtual environment** (**venv**, **virtualenv**, ...), see this good `tutorial <https://realpython.com/python-virtual-environments-a-primer/>`_ for more information

- *conda*: both Python packages and non-Python libraries

.. warning::

    Use containers for complete isolation and full reproducibility of the software environment (**docker**, **singularity**, **podman**, ...)

Here we will use **venv** + **pypi** to set up our environment.

- **venv**: integrated in the Python standard library since version 3.6 (but on some OS's it must be installed separately) `https://docs.python.org/3/library/venv.html <https://docs.python.org/3/library/venv.html>`_

- *virtualenv*: can be installed via pip or your package manager/ `https://virtualenv.pypa.io/ <https://virtualenv.pypa.io/>`_ as an alternative if ``venv`` is not available

Create a virtual environment ``env`` inside your project

.. code:: sh

    mkdir -p strumenti_informatici/lezione_2
    cd strumenti_informatici/lezione_2
    python -m venv env

Load the virtual environment (note the dot at the beginning of the line!)

.. code:: sh

    . env/bin/activate

Install packages from the official Python project repository `pypi <https://pypi.org>`_ in your virtual environment.

.. code:: sh

    pip install matplotlib
    # Alternative
    # python -m pip install matplotlib

We can ensure that a specific ``X.Y.Z`` version is installed

.. code:: sh

    pip install matplotlib==3.8.0

::

    Requirement already satisfied: matplotlib==3.8.0 in ./env/lib/python3.10/site-packages (3.8.0)
    =1.0.1 in ./env/lib/python3.10/site-packages (from matplotlib==3.8.0) (1.1.1)
    =4.22.0 in ./env/lib/python3.10/site-packages (from matplotlib==3.8.0) (4.42.1)
    =1.21 in ./env/lib/python3.10/site-packages (from matplotlib==3.8.0) (1.26.0)
    =6.2.0 in ./env/lib/python3.10/site-packages (from matplotlib==3.8.0) (10.0.1)
    =2.3.1 in ./env/lib/python3.10/site-packages (from matplotlib==3.8.0) (3.1.1)
    =2.7 in ./env/lib/python3.10/site-packages (from matplotlib==3.8.0) (2.8.2)
    =1.0.1 in ./env/lib/python3.10/site-packages (from matplotlib==3.8.0) (1.4.5)
    =20.0 in ./env/lib/python3.10/site-packages (from matplotlib==3.8.0) (23.1)
    =0.10 in ./env/lib/python3.10/site-packages (from matplotlib==3.8.0) (0.11.0)
    =1.5 in ./env/lib/python3.10/site-packages (from python-dateutil>=2.7->matplotlib==3.8.0) (1.16.0)

For portability, we can provide a ``requirements.txt`` file with lines like

.. code:: sh

    matplotlib==3.8.0

Then, the dependencies can be installed with ``pip install -r requirements.txt``.

Get the currently installed packages (useful to recreate the full environment via a `requirements.txt file <https://pip.pypa.io/en/stable/user_guide/#requirements-files>`_)

.. code:: sh

    pip freeze

::

    contourpy==1.1.1
    cycler==0.11.0
    fonttools==4.42.1
    kiwisolver==1.4.5
    matplotlib==3.8.0
    numpy==1.26.0
    packaging==23.1
    Pillow==10.0.1
    pyparsing==3.1.1
    python-dateutil==2.8.2
    six==1.16.0

.. tip::

    The output of ``pip freeze`` dumps all the installed packages, also those installed as dependencies. To get only the "root" packages, which were actually installed by the user, have a look at the package ``pipdeptree`` available on ``pypi``.

Note that ``pip`` can install also `local or remote Python projects <https://pip.pypa.io/en/stable/cli/pip_install/#vcs-support>`_ (over a network, on a remote git server, ...)

.. code:: sh

    # A local version of matplotlib
    pip install ~/usr/matplotlib

    # A specific tag / branch / commit of matplotlib from github
    pip install git+https://github.com/matplotlib/matplotlib@v3.8.0

Finally deactivate the virtual environment

.. code:: sh

    deactivate

A matter of style
~~~~~~~~~~~~~~~~~

The golden rules to write top-quality Python code are crystallized in `PEP-8 <http://legacy.python.org/dev/peps/pep-0008/>`_

::

    "One of Guido's key insights is that code is read much more often than
    it is written. The guidelines provided here are intended to improve
    the readability of code and make it consistent across the wide
    spectrum of Python code. As PEP 20 says, Readability counts."

While Python tolerates ad-hoc custom conventions on spacings, name conventions etc, the PEP-8 provides the authoritative reference to write good Python code. There are package and tools that help you clean up your code automatically, such as ``flake8``, ``autopep8``, ``black``...

A matter of philosophy
~~~~~~~~~~~~~~~~~~~~~~

The philosophy of Python coding is beautifully described by the ``Zen of Python`` (**PEP-20**), which you should deeply ponder every night before going to sleep

::

    Beautiful is better than ugly.
    Explicit is better than implicit.
    Simple is better than complex.
    Complex is better than complicated.
    Flat is better than nested.
    Sparse is better than dense.
    Readability counts.
    Special cases aren't special enough to break the rules.
    Although practicality beats purity.
    Errors should never pass silently.
    Unless explicitly silenced.
    In the face of ambiguity, refuse the temptation to guess.
    There should be one -and preferably only one- obvious way to do it.
    Although that way may not be obvious at first unless you're Dutch.
    Now is better than never.
    Although never is often better than right now.
    If the implementation is hard to explain, it's a bad idea.
    If the implementation is easy to explain, it may be a good idea.
    Namespaces are one honking great idea - let's do more of those!

Compiled vs. interpreted languages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

From `https://en.wikipedia.org/wiki/Compiler <https://en.wikipedia.org/wiki/Compiler>`_:

::

    In computing, a compiler is a computer program that translates
    computer code written in one programming language (the source
    language) into another language (the target language).

From `https://en.wikipedia.org/wiki/Interpreter_(computing) <https://en.wikipedia.org/wiki/Interpreter_(computing)>`_:

::

    In computer science, an interpreter is a computer program that
    directly executes instructions written in a programming or scripting
    language, without requiring them previously to have been compiled into
    a machine language program.

.. table::

    +-------------------+-----------------------------------+--------------------------------------+
    | \                 | **Compiled**                      | **Interpreted**                      |
    +===================+===================================+======================================+
    | **Advantages**    | Fast execution                    | Dynamic types; platform independence |
    +-------------------+-----------------------------------+--------------------------------------+
    | **Disadvantages** | Slow testing; platform dependence | Translation overhead at run time     |
    +-------------------+-----------------------------------+--------------------------------------+

Actually, Python code is typically first translated to bytecode, then interpreted. But it can also be compiled "just-in-time" (for instance with `pypy <https://doc.pypy.org/en/latest/>`_, which claims to be on average ~5 times faster than the official ``CPython`` distribution).

Development environment
~~~~~~~~~~~~~~~~~~~~~~~

The **Python interpreter** executes the commands (assignments, control flow statements, etc.) one at a time. Start it by typing ``python`` (or ``python3``) from a terminal. You can then type commands interactively and then quit the interpreter with ``ctrl-d`` or typing ``exit()``. There is also an **enhanced** version of the Python interpreter called **ipython**, which adds tab completion, history and extra features.

`Jupyter notebooks <https://jupyter.org/>`_ (born in 2015) are the evolution of ``ipython``, with a Mathematica-like interface. They can be accessed and executed interactively from a web browser. They are not limited to the Python language (via different language **kernels**).

.. image:: ./jupyterpreview.webp

**Features**:

- Work locally in a browser; remote execution via **jupyterhub** or in the cloud (**binder**)

- Store notebook as a formatted, xml file (``.ipynb``)

- **Jupyterlab** provides better integration between notebooks

- Export project in html/pdf and other formats

*Shortcomings*:

- Encourage a monolithic coding style

- Cumbersome to use in bare-bones environments (ex. HPC clusters)

- Lack of customizability and ergonomy (at least, IMHO)

We install ``jupyter`` in our virtual environment

.. code:: sh

    . env/bin/activate
    pip install jupyter

Then we can launch ``jupyter`` from the command line

.. code:: sh

    jupyter notebook

`JupyterLab <https://jupyterlab.readthedocs.io/en/stable/getting_started/overview.html>`_ provides a richer development environment for Jupyter notebooks, giving access to text editors, terminals, and custom components in a integrated way. It comes installed with the jupyter package.

.. image:: ./interface-jupyterlab.png

We can launch ``jupyterlab`` from the command line

.. code:: sh

    jupyter lab

Remember to deactivate the environment once you are done

.. code:: sh

    deactivate

Python **scripts and modules** are easier to develop using a **code editor** or an **integrated development environment:**

- **Code editors**: `emacs <https://emacsdocs.org/>`_ (since 1976), `vim <https://www.vim.org/>`_ (since 1976, through 1991), ...

- **IDEs**: `VS code <https://code.visualstudio.com/>`_ (since 2015), `atom <https://atom.io>`_ (2014-2022), `sublime <https://www.sublimetext.com/>`_ (since 2008), `Xcode <https://developer.apple.com/xcode/>`_ (since 2003), ...

You can edit and execute also jupyter notebooks within most of them!

A few useful shortcuts in Jupyter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Switch between **editing** and **browsing** mode: ``Esc``

- **Run** selected cell and **advance**: ``Shift-Enter``

- **Run** selected cell and **do not advance**: ``Ctrl-Enter``

- **Indent** a block of code: select the lines, then ``Tab``

- **Unindent** a block of code: select the lines, then ``Alt-Tab``

- **Comment** a block of code: select the lines, then ``Ctrl-/`` (or ``Cmd+/`` on macOS)

.. warning::

    Do not use triple quotes to massively comment regions of code! Just select and comment them.

Fortran-Python Rosetta stone
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For Fortran 90 programmers, there is a useful `Rosetta stone <https://www.fortran90.org/src/rosetta.html>`_ to map Python syntax and numpy arrays features to Fortran 90. Indeed, it turns out that

::

    Python with NumPy and Fortran are actually rather similar in terms of
    expressiveness and features

but Fortran is generally faster.

Quick start
-----------

This tutorial is also available as a `jupyter notebook <https://framagit.org/coslo/sinfo/-/raw/master/python/basics.ipynb>`_ or on `binder <https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fcoslo%2Fsinfo/HEAD?labpath=python%2F>`_. 

Scalars
~~~~~~~

In Python there are **objects** of integer, float and string type. There is much more of course, but that's all we need for the time being.

.. code:: python

    # Number of particles
    N = 1
    # Scalar distance between two particles
    r = 1.0
    # A simple string
    feature = 'potential energy'

    print(N, type(N))
    print(r, type(r))
    print(feature, type(feature))

::

    1 <class 'int'>
    1.0 <class 'float'>
    potential energy <class 'str'>


.. note::

    In Python, almost everything is an object - in the sense of `object-oriented programming <https://en.wikipedia.org/wiki/Object-oriented_programming>`_. An object combines variables and methods that operate on those variables. We'll cover this quickly later on in the tutorials.

We can get more control on formatting the output of ``print()`` with the powerful `f-strings <https://docs.python.org/3/reference/lexical_analysis.html#f-strings>`_

.. code:: python

    print(f'what is the value of the {feature} when r={r}?')

::

    what is the value of the potential energy when r=1.0?


The canonical way to format strings in Python is a bit more verbose but more flexible

.. code:: python

    print('what is the value of the {} when r={}?'.format(feature, r))
    print('what is the value of the {1} when r={0}?'.format(r, feature))
    print('what is the value of the {what} when r={distance}?'.format(what=feature, distance=r))

Formatting can get fairly sophisticated

.. code:: python

    # Center string and fill line with dashes
    print('{:-^30}'.format(feature))

    # The square root of 2 is printed with 6 digits and left justified
    r = 2.0**0.5
    print(f'r={r:<.6g}')

::

    -------potential energy-------
    r=1.41421


Have a look at the `Python documentation <https://docs.python.org/3/library/string.html#formatstrings>`_ for more details on string formatting.

Lists and tuples
~~~~~~~~~~~~~~~~

Beyond scalars, in physics we need vectors. A flexible (though generally inefficient) way to manipulate vector data in Python is through `lists <https://docs.python.org/3/library/stdtypes.html#list>`_. They behave similarly to array in compiled languages like Fortran, but there are some key differences. Compared to arrays, lists have one main **advantage**

- elements in a list can be added and removed efficiently

and one main **drawback**

- accessing elements of a list takes way more time than you would expect from an array

With these ideas in mind, let us have a look at the syntax to manipulate a list object, which will store the distances at which we will compute the interaction potential :math:`u(r)`

.. code:: python

    # A list of integers
    r = [1, 2, 3]
    print('Number of elements:', len(r))

    # Add an element
    r.append(4)
    print(r)

    # Remove the first occurrence of element 2 from the list
    r.remove(2)
    print(r)

    # Find the index of element 3 in the list
    print(r.index(3))

::

    Number of elements: 3
    [1, 2, 3, 4]
    [1, 3, 4]
    1


.. note::

    Indices in Python start from 0, like in C.

.. tip::

    In the examples above, ``append()`` and ``remove()`` are **methods** of the **object** ``r``. To get help on the possible methods of an object, type ``help(r)`` from an interactive Python session or get help within your IDE.

.. code:: python

    help(r)

Accessing individual entries of a list can be done with a familiar "array-like" syntax

.. code:: python

    r[0] = 1.0  # first element
    r[-1] = 2.0  # last element
    print(r[-1] - r[0])

::

    -1.0


There is a useful shortcut to generate a list of objects all equal to one another

.. code:: python

    N = 5
    r = [0.0] * N
    print('The list x has', len(r), 'elements')

::

    The list x has 5 elements


Finally, there is another type of object to hold collections of data: `tuples <https://docs.python.org/3/library/stdtypes.html#tuple>`_.

.. code:: python

    data_as_tuple = (0, 10.0, "hello")
    data_as_list = (0, 10.0, "hello")

Notice how both lists and tuples can store objects of different types. Contrary to lists, however, tuples cannot be modified once created.

**Exercise**: create a list of tuples to store matrix-like data. This will be useful later on to store the positions :math:`\{\vec{r}_i\}` of the particles, with :math:`i=1, \dots, N`. How can you access a given element of a given tuple of the list? 

Functions
~~~~~~~~~

Let us define the Lennard-Jones potential as a function

.. code:: python

    def lennard_jones(r, epsilon=1.0, sigma=1.0):
        """Lennard-Jones potential. By default, `sigma` and `epsilon` are  1."""
        return 4 * epsilon * ((sigma/r)**12 - (sigma/r)**6)

    # Different ways to evaluate the function
    print(lennard_jones(1.0), lennard_jones(1.2, sigma=1.0, epsilon=1.0))

::

    0.0 -0.8909652875830761


To identify the lines of code that will be executed within the body of the function (or of `other control flow <https://docs.python.org/3/tutorial/controlflow.html>`_ statements), Python uses a strong convention on **code indentation**. This is probably one of the biggest shift if you come from a language like C or Fortran.

Typically, 4 spaces are used to separate code blocks. When the block is over, we get back to the previous indentation level. When you go to a new line after a loop or an if, your **editor / IDE** will automatically indent the line as needed or change indentation level with a shortcut (if not, change editor!). Every editor / IDE has its own way to deal with indentation.

.. note::

    It is good practice to document the purpose of the function with a `docstring <https://peps.python.org/pep-0257/>`_ enclosed by triple apostrophes as above. Check out the `pyment <https://github.com/dadadel/pyment>`_ package to convert between different doc-string conventions.

If we want to return both the potential and its first derivative, we can return two variables

.. code:: python

    def lennard_jones_first(r, epsilon=1.0, sigma=1.0):
        u = lennard_jones(r, epsilon=epsilon, sigma=sigma)
        uu = 0.0  # code the derivative here...!
        return u, uu

When calling this function, we can unpack the results like

.. code:: python

    u, uu = lennard_jones_first(1.2)
    print(f'potential={u:.2f} derivative={uu:.2f}')

::

    potential=42.95 derivative=0.00

*The dark side*
~~~~~~~~~~~~~~~

A highly recommended reading: `Facts and myths about Python names and values <https://nedbatchelder.com/text/names.html>`_

Variables are references to objects
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As we have seen above, variables in Python behave differently then in statically-typed languages like C or Fortran. 

.. code:: python

    x = 1
    print(x, type(x))
    x = 'hello world'
    print(x, type(x))

::

    1 <class 'int'>
    hello world <class 'str'>


Here it looks like ``x`` is

1. born as an integer

2. but then becomes a string

What is ``x``, actually? ``x`` is a variable that first refers to object ``1`` and then the object ``'hello world'``. Almost everything in Python is an "object" and variables ("names") are just references ("bindings") to them. To sum up: **variables are references to objects** (or "names are bindings to objects").

Get the identifier of the object referenced by ``x`` with the ``id()`` function

.. code:: python

    x = 1
    print(id(x))

::

    139994027262192


Now increment ``x`` by one

.. code:: python

    x = x + 1
    print(id(x), id(2))

::

    139994027262224 139994027262224


What happened? ``x`` refers to a new object (``2``) with a different id. We did not *modify* an integer variable, we created a new one and referred ``x`` to it! This is because in Python integers are "immutable" objects.

Mutable and immutable objects
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

An object is **mutable** if its internal variables can be modified *in-place* (without creating a new object) and **immutable** otherwise. Immutable objects can only be reassigned, not modified. Integers, floats and strings are examples of immutable objects: you cannot change them, only create new ones if they do not exist yet.

Another classic example of mutable vs. immutable data structures in Python are **lists** and **tuples.**

Lists can be modified in-place using the ``[...]`` syntax (they are "subscriptable") 

.. code:: python

    mutable = [0, 1, 2]
    mutable[0] = "x"
    print(mutable)

::

    ['x', 1, 2]


But tuples are immutable so you'll get an error

.. code:: python

    not_mutable = (0, 1, 2)  # A tuple
    not_mutable[0] = "x"

::

    Traceback (most recent call last):
        not_mutable[0] = "x"
    TypeError: 'tuple' object does not support item assignment

Non-locality
^^^^^^^^^^^^

OK, this can be tricky. Here

.. code:: python

    x = [1, 2]
    y = x

we understand that both ``x`` and ``y`` refer to the same list

.. code:: python

    print(id(x) == id(y), x is y)

::

    True True


When we add an element to ``x``, ``y`` is modified as well, because ``x`` is modified *in-place* and ``y`` still refers to the very same object

.. code:: python

    x.append(3)
    print(x, y)

::

    [1, 2, 3] [1, 2, 3]


This behavior holds in general for mutable objects and can be quite confusing, in particular when passing arguments to *functions* or when complex objects *share* data.

However, the behavior is different with *immutable* objects.

.. code:: python

    x = 1
    y = x

The above statements bind again ``y`` to the same object as ``x``

.. code:: python

    print(id(x) == id(y), x is y)

::

    True True


But when we increment ``x,`` we actually *bind* it to the object ``2`` while ``y`` still refers to ``1``

.. code:: python

    x += 1
    print(x, y)

::

    2 1


The connection with ``y`` is thus lost.

Making copies of objects
^^^^^^^^^^^^^^^^^^^^^^^^

Given the above situation, it can be useful to make copies of an object. For lists, it is enough to use

.. code:: python

    z = list(x)

In a more general situation, however, we use the `copy <https://docs.python.org/3/library/copy>`_ module for that.

.. note::

    Modules in Python are files containing reusable code. Many modules are shipped with the `Python standard library <https://docs.python.org/3/library/>`_, but you can easily create your own.

After importing the module, we can use the two functions defined within it: ``copy`` and ``deepcopy``.

.. code:: python

    import copy

    x = [1, 2]
    y = copy.copy(x)
    print(x is y)
    x.append(3)
    print(x, y)

::

    False
    [1, 2, 3] [1, 2]


Keep in mind that ``copy()`` only makes a "shallow" copy: if the object contains other objects that hold references inside, the objects pointed by those references will not be copied. To make sure the object is *fully* copied, use ``deepcopy()`` instead

.. code:: python

    y = copy.deepcopy(x)

**Exercise**: in the case of immutable object, we get the same results with ``copy`` and ``deepcopy``. To try out the difference, make copies of nested lists, that is a list of lists, and check the ``id`` of an element of the list after ``copy`` and ``deepcopy``.

Loops and conditions
~~~~~~~~~~~~~~~~~~~~

Let us introduce some basic **flow control** structures: loops and if's.

To see how a loop looks like in Python, we iterate over the elements of a list

.. code:: python

    for i in [0, 1, 2]:
        print(i)

To identify the lines of code that will be executed within the body of a `for <https://docs.python.org/3/tutorial/controlflow.html#for-statements>`_ loop (or of `other control flow <https://docs.python.org/3/tutorial/controlflow.html>`_ statements), Python uses again a strong convention on **code indentation**.

Let's add an `if <https://docs.python.org/3/tutorial/controlflow.html#if-statements>`_ condition

.. code:: python

    for i in [0, 1, 2]:
        if i == 0:
            # We do nothing and jump to next iteration
            continue
        elif i % 2 == 1:
            print(i, 'is even')
        else:
            print(i, 'is odd, we get out of the loop')
            break
        # Now the if block is over
    # Now the for block is over

::

    1 is even
    2 is odd, we get out of the loop


Have you noticed how nested **blocks are indented**?

Looping over a range of integers can be done with the `range <https://docs.python.org/3/library/stdtypes.html#ranges>`_ function

.. code:: python

    for i in range(10):
        pass  # do nothing

The variable ``i`` above ranges from 0 to 9

.. code:: python

    print(list(range(10)))

::

    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


More control on the range

.. code:: python

    start, stop, increment = 10, 0, -1
    print(list(range(start, stop, increment)))

::

    [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]


The powerful `list comprehension <https://docs.python.org/3/tutorial/datastructures.html?highlight=list%20comprehension#list-comprehensions>`_ syntax allows us to generate lists in a compact way

.. code:: python

    N = 5
    r = [i**2 for i in range(N)]
    print(r)

::

    [0, 1, 4, 9, 16]


The above syntax will create the whole list and store it in memory. If ``N`` is large this can be an issue. There is a list `generator <https://docs.python.org/3/glossary.html#term-generator>`_ for this

.. code:: python

    r = (i**2 for i in range(N))
    print(r)

::

    <generator object <genexpr> at 0x7fb4ad1e8580>


.. note::

    The function ``range()`` actually returns a generator - that's why above we wrapped ``list()`` around it in order to print the list of distances!

Finally, a convenient function is `enumerate <https://docs.python.org/3/library/functions.html?highlight=enumerate#enumerate>`_, which returns the entries of a list along with the index

.. code:: python

    x = [i**2 for i in range(5)]
    for i, xi in enumerate(x):
        print(i, xi, x[i])

::

    0 0 0
    1 1 1
    2 4 4
    3 9 9
    4 16 16


.. note::

    Accessing the list elements in the loop block above as ``xi`` is a more "pythonic" idiom than ``x[i]``.

It is possible to iterate over multiple lists element by element "in sync" using the `zip <https://docs.python.org/3/library/functions.html?highlight=zip#zip>`_ intrinsic function

.. code:: python

    y = [i**3 for i in range(5)]
    for xi, yi in zip(x, y):
        z = xi + yi

I/O
~~~

Compared to languages like C and Fortran, reading and writing data from or to files is a piece of cake.

Let us start by writing some data to a file.

.. code:: python

    N = 5
    x = [1.0] * N
    y = [2.0] * N

    # Open file in write mode ('w')
    with open('/tmp/data.txt', 'w') as fh:
        for i in range(N):
            fh.write(f'{x[i]} {y[i]}\n')

Remember to add a newline with ``\n.``

Let us read the data back. We use the `split <https://docs.python.org/3/library/stdtypes.html#str.split>`_ method of strings to split each line according to the white space and get the result as a tuple.

.. code:: python

    x, y = [], []

    # Default is read mode ('r')
    with open('/tmp/data.txt') as fh:
        for line in fh:
            data = line.split()
            x.append(data[0])
            y.append(data[1])

    # Check the data
    print(f'x = {x} y = {y}')

::

    x = ['1.0', '1.0', '1.0', '1.0', '1.0'] y = ['2.0', '2.0', '2.0', '2.0', '2.0']


Note that the ``line`` variable is a string and so are the elements of ``data``. If we want to store lists of floats, we must cast the variables explicitly, using ``float(data[0])`` and ``float(data[1])``.

.. note::

    Writing and reading columnar data can be done more easily with a few functions defined in the numpy package. For more general and complex data structures, the `pickle <https://docs.python.org/3/library/>`_ module can be quite handy - although it has its own drawbacks.

Plotting data
~~~~~~~~~~~~~

Let us tabulate the Lennard-Jones potential on a grid of distances

.. code:: python

    def tabulate(r_grid):
        u = []
        for r in r_grid:
            u.append(lennard_jones(r))
        return u

    # Define grid
    N = 50
    r_min, r_max = 0.9, 3.0
    dr = (r_max - r_min) / (N - 1)
    r_grid = [r_min + dr*i for i in range(N)]

    # Tabulate the potential
    u_grid = tabulate(r_grid)

Let us quickly plot the results with `matplotlib <https://matplotlib.org/>`_

.. code:: python

    import matplotlib.pyplot as plt

    plt.plot(r_grid, u_grid, label='LJ potential')
    plt.xlabel("r")
    plt.ylabel("u(r)")
    plt.legend()
    plt.show()  # not needed in jupyter

    # We can save the plot as a file in various formats
    plt.savefig('plot.png')
    plt.savefig('plot.pdf')

.. image:: ./plot.png

Check out the `matplotlib gallery <https://matplotlib.org/stable/plot_types/index.html>`_ for possible types of plot!

Exercise
--------

**Nanoparticle setup**

Write a Python code that creates a piece of `face-centered cubic <https://en.wikipedia.org/wiki/Cubic_crystal_system>`_ (fcc) crystal. The sample should be large enough, say at least :math:`N=1000` particles. The units of distance is the :math:`\sigma` parameter of the Lennard-Jones potential.

1. Store the coordinates :math:`{\vec{r}_i}`, for :math:`i=1, \dots, N` of the particles in a list (as a list of tuples or lists)

2. Compute the center of mass (CM) of the crystal sample

3. Remove from the crystal all the particles whose distance from the CM is larger than a threshold, say 3.0 in units of sigma

4. Store the nanoparticle configuration in a file (each particle position on a separate line)
