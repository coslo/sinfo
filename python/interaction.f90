subroutine potential(r, u)
  double precision, intent(in) :: r
  double precision, intent(out) :: u
  u = 4 * (1/r**12 - 1/r**6)
end subroutine potential

subroutine total_potential_energy(pos, U)
  double precision, intent(in) :: pos(:,:)
  double precision, intent(out) :: U
  double precision :: rij, r(size(pos,2)), uij
  U = 0.0
  do i = 1,size(pos, 1)
    do j = i+1,size(pos, 1)
      r = pos(i, :) - pos(j, :)
      rij = sum(r**2)**0.5
      call potential(rij, uij)
      U = U + uij
    end do
  end do
end subroutine total_potential_energy

subroutine total_potential_energy_forder(pos, U)
  double precision, intent(in) :: pos(:,:)
  double precision, intent(out) :: U
  double precision :: rij, r(size(pos,1)), uij
  U = 0.0
  do i = 1,size(pos, 2)
    do j = i+1,size(pos, 2)
      r = pos(:, i) - pos(:, j)
      rij = sum(r**2)**0.5
      call potential(rij, uij)
      U = U + uij
    end do
  end do
end subroutine total_potential_energy_forder
