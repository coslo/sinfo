module potentials
  implicit none
contains
  subroutine potential(r, u)
    double precision, intent(in) :: r
    double precision, intent(out) :: u
    u = 4 * (1/r**12 - 1/r**6)
  end subroutine potential
end module potentials
