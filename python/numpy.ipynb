{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "ec7579fd",
   "metadata": {},
   "source": [
    "# What we\\'ll cover today\n",
    "\n",
    "-   Basics of **NumPy** for scientific computing\n",
    "-   When are operations on numpy arrays **efficient** and when not?\n",
    "-   Super-easy formatted **I/O** with **numpy**\n",
    "\n",
    "# Introduction to numpy\n",
    "\n",
    "The `numpy` package provides general n-dimensional arrays to\n",
    "do efficient linear algebra on matrices of arbitrary dimensions. It is\n",
    "the building block of most of the scientific computing done with Python\n",
    "nowadays.\n",
    "\n",
    "## Constructing an array\n",
    "\n",
    "Equivalent ways to create a 3d array, filled with zeros\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4c99f0a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "# From a list (or iterable)\n",
    "np.array([0.0, 0.0, 0.0])\n",
    "\n",
    "# Using the numpy.zeros function\n",
    "np.zeros(3)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "e6f4f7b2",
   "metadata": {},
   "source": [
    "The `dtype` argument gives us control on the [datatype and\n",
    "precision](https://numpy.org/doc/stable/reference/arrays.dtypes.html).\n",
    "These are equivalent ways to get an array of floating point numbers in\n",
    "double precision\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9c254cd1",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.zeros(3)\n",
    "np.zeros(3, dtype=float)\n",
    "np.zeros(3, dtype=np.float)\n",
    "np.zeros(3, dtype=np.float64)\n",
    "np.zeros(3, dtype='d')\n",
    "np.zeros(3, dtype=np.dtype('d'))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "427676ba",
   "metadata": {},
   "source": [
    "Arrays have some useful attributes, among them let me mention\n",
    "\n",
    "-   `shape`: a tuple describing the size of the array along\n",
    "    each of its dimensions\n",
    "-   `dtype`: the datatype of the array\n",
    "\n",
    "Check the size and datatype of our position array\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1346eca3",
   "metadata": {},
   "outputs": [],
   "source": [
    "pos = np.zeros(3)\n",
    "print(pos.shape, pos.dtype)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "38b8f265",
   "metadata": {},
   "source": [
    "Create an array of integer with single precision (32 bits, compatible\n",
    "with Fortran default integer precision) and inspect the largest possible\n",
    "value\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5e11cb56",
   "metadata": {},
   "outputs": [],
   "source": [
    "ids = np.array([1, 2, 3], dtype=np.int32)\n",
    "print(np.iinfo(ids.dtype))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "3534e704",
   "metadata": {},
   "source": [
    "We can create [empty\n",
    "arrays](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html)\n",
    "of any shape and type, to do linear algebra on matrices of arbitrary\n",
    "dimensions\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "321fd8a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Number of particles\n",
    "N, ndim = 10,  3\n",
    "pos = np.ndarray((N, ndim))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "5ad59efd",
   "metadata": {},
   "source": [
    "See the\n",
    "[documentation](https://numpy.org/doc/stable/reference/generated/numpy.array.html)\n",
    "for more details about creating numpy arrays.\n",
    "\n",
    "## Accessing and modifying array elements\n",
    "\n",
    "To access and modify array elements, we use the same **subscript\n",
    "syntax** as for lists and tuples, like `pos[0]`. Dealing with\n",
    "multi-dimensional arrays requires some further explanation\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ae69785b",
   "metadata": {},
   "outputs": [],
   "source": [
    "pos = np.ndarray((3, 3))\n",
    "\n",
    "# Access the third element of the first entry of pos\n",
    "# Equivalent ways\n",
    "print(pos[0][2])\n",
    "print(pos[0, 2])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "0827aecb",
   "metadata": {},
   "source": [
    "By default, Python uses **C order** for storing multidimensional arrays\n",
    "in memory: arrays are stored by **row** so that elements are contiguous\n",
    "in memory along the \\\"last\\\" axis ([row major\n",
    "order](https://en.wikipedia.org/wiki/Row-_and_column-major_order)).\n",
    "\n",
    "\n",
    "This is important when interfacing code to Fortran (see later). To\n",
    "provide a smooth passage of arrays between Python and Fortran, we can\n",
    "arrange the arrays in **Fortran order** instead ([column major\n",
    "order](https://en.wikipedia.org/wiki/Row-_and_column-major_order))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e47b787b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# order can be 'C' or 'F'\n",
    "pos = np.ndarray((3, 3), order='F')\n",
    "print(pos.flags['F_CONTIGUOUS'])\n",
    "\n",
    "# We can transform an array in F order\n",
    "pos = np.asfortranarray(np.ndarray((3, 3)))\n",
    "print(pos.flags['F_CONTIGUOUS'])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "335be516",
   "metadata": {},
   "source": [
    "Numpy arrays have powerful Fortran-like **vector syntax** to operate on\n",
    "arrays. We showcase it by reimplementing the calculation of the center\n",
    "of mass of the system. We assign the following coordinates to the\n",
    "particles:\n",
    "\n",
    "-   $\\vec{r}_0 = (0, 0, 0)$\n",
    "-   $\\vec{r}_1 = (1, 1, 1)$\n",
    "-   $\\vec{r}_2 = (-1, -1, 1)$\n",
    "\n",
    "so that the CM is at $(0, 0, 0)$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "362f162f",
   "metadata": {},
   "outputs": [],
   "source": [
    "N, ndim = 3, 3\n",
    "mass = np.ones(N)\n",
    "pos = np.zeros((N, ndim))\n",
    "\n",
    "# Adjust particle positions\n",
    "pos[1, :] = 1.0\n",
    "pos[2, :] = -1.0\n",
    "\n",
    "# Compute CM\n",
    "cm = np.ndarray(ndim)\n",
    "cm[:] = 0.0  # note the [:] here\n",
    "for i in range(pos.shape[0]):\n",
    "    cm += pos[i, :] * mass[i]\n",
    "cm /= sum(mass)\n",
    "\n",
    "print(cm)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "70c56df4",
   "metadata": {},
   "source": [
    "**Warning**:\n",
    "Initializing the CM position as =cm = 0.0= will give the same final result, but it is not quite correct. Make sure you understand the difference wrt =cm[:]=0.0=!"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "35362e3b",
   "metadata": {},
   "source": [
    "It is possible to operate on array subsections using a powerful\n",
    "**slicing** syntax\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f896b627",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print the positions of the first two particles\n",
    "print(pos[0: 2, :])\n",
    "\n",
    "# Copy the first position into the last one\n",
    "pos[-1, :] = pos[0, :]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "47b9818e",
   "metadata": {},
   "source": [
    "The \\\"vector syntax\\\" expresses the fact that some assignments can be\n",
    "performed **element-wise**. Fortran programmers will recognize the\n",
    "familiar syntax for vector operation, which, indeed, was introduced in\n",
    "Fortran years before `numpy` was born! Follow the [Rosetta\n",
    "stone](https://www.fortran90.org/src/rosetta.html) for details about\n",
    "matching NumPy and Fortran syntax.\n",
    "\n",
    "## Zero-dimensional arrays\n",
    "\n",
    "Numpy also provides **0-dimensional arrays** as scalars. Contrary to\n",
    "regular float and int types in Python, 0-dimensional arrays are\n",
    "*mutable*. We declare the Planck constant as a 0-dimensional array\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "42f4287b",
   "metadata": {},
   "outputs": [],
   "source": [
    "h = np.array(6.62607015)  # in units of J/s\n",
    "print(h, h.shape, type(h), id(h))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61578d3a",
   "metadata": {},
   "source": [
    "**Note**:\n",
    "The shape of a 0-array is an empty tuple."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "73537032",
   "metadata": {},
   "source": [
    "We scale $h$ **in-place** using the `/=` syntax\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "931c63ae",
   "metadata": {},
   "outputs": [],
   "source": [
    "h /= (2*np.pi)\n",
    "print(h, type(h), id(h))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "88a51921",
   "metadata": {},
   "source": [
    "There is no re-referencing here: we have modified the state of the\n",
    "object. This can be useful later on when interfacing Python with Fortran\n",
    "code.\n",
    "\n",
    "## Mutability of arrays\n",
    "\n",
    "Numpy arrays are mutable and subscriptable: it is crucial, however, to\n",
    "use the `[...]` syntax for in-place modifications\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4dedfc7f",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.array([0, 1, 2])\n",
    "x[0] = 1  # subscriptable\n",
    "print(x, id(x))\n",
    "x[:] = x + 1  # in-place modification\n",
    "print(x, id(x))\n",
    "x += 1  # in-place modification\n",
    "print(x, id(x))\n",
    "x = x + 1  # reassignment!\n",
    "print(x, id(x))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "cafd6fa9",
   "metadata": {},
   "source": [
    "For 0-dimensional arrays, mutability may lead to non-local effects not\n",
    "occurring with regular float\\'s and int\\'s\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e90592d8",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.array(1)\n",
    "y = x\n",
    "x += 1\n",
    "print(x, y)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "81db572e",
   "metadata": {},
   "source": [
    "Of course, that\\'s because the modification is done in-place and both\n",
    "`x` and `y` refer to the same object.\n",
    "\n",
    "To check if two arrays share *some* data, use\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "76d3556f",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(np.shares_memory(x, y))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "4e5c1fc0",
   "metadata": {},
   "source": [
    "## Numpy functions and array methods\n",
    "\n",
    "Numpy arrays have a number of useful\n",
    "[methods](https://numpy.org/doc/stable/reference/arrays.ndarray.html#array-methods)\n",
    "and built-in functions to [manipulate\n",
    "them](https://numpy.org/doc/stable/reference/routines.array-manipulation.html).\n",
    "We will only cover two examples, which will be useful to rewrite the\n",
    "nano-particle setup using numpy arrays.\n",
    "\n",
    "### Summing array elements\n",
    "\n",
    "The sum of the elements of a list can be done with the `sum`\n",
    "intrinsic function. This also works with numpy arrays, because they are\n",
    "iterable\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6d221052",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Total mass:', sum(mass))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "19bbdb60",
   "metadata": {},
   "source": [
    "There are intrinsic numpy functions and methods to do the same\n",
    "operation, possibly in a more efficient way (see below!)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4bc1b7cf",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.sum(mass)  # function\n",
    "mass.sum()  # method"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "37ea690d",
   "metadata": {},
   "source": [
    "### Adding elements to an array\n",
    "\n",
    "It is possible to [add and remove elements of numpy\n",
    "arrays](https://numpy.org/doc/stable/reference/routines.array-manipulation.html#adding-and-removing-elements)\n",
    "in a similar way as with lists. However, this cannot be done in-place\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "06cfb890",
   "metadata": {},
   "outputs": [],
   "source": [
    "mass.append(1)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "64d920d4",
   "metadata": {},
   "source": [
    "We must instead create a new array with the `append` function\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15dc3db5",
   "metadata": {},
   "outputs": [],
   "source": [
    "mass = np.ones(3)\n",
    "mass_new = np.append(mass, [1.0])\n",
    "print('It should be 3+1:', mass_new.shape[0])\n",
    "\n",
    "pos = np.zeros((2, 3))\n",
    "pos_new = np.append(pos, [[1.0, 1.0, 1.0]], axis=0)\n",
    "print('It should be (2+1, 3):', pos_new.shape)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "676d1dc8",
   "metadata": {},
   "source": [
    "Notice that the `append` function returns a **new array**.\n",
    "When arrays are large, appending creating new arrays with\n",
    "`append` may be time consuming, and it may be better to first\n",
    "create a list and then convert it to an array with `array()`.\n",
    "\n",
    "## I/O with numpy\n",
    "\n",
    "Finally, `numpy` provides high-level functions to\n",
    "**read/write data** from/to files at with minimal effort.\n",
    "\n",
    "To write the positions to file\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c3767a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "def lattice(N):\n",
    "    return np.ones((N, 3))\n",
    "pos = lattice(10)\n",
    "np.savetxt('data.txt', pos)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "c35173c7",
   "metadata": {},
   "source": [
    "Check out the first 5 lines of the file\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "76276ffd",
   "metadata": {},
   "outputs": [],
   "source": [
    "! head -n 5 data.txt"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "eafa2ba3",
   "metadata": {},
   "source": [
    "The `savetxt()` function provides various arguments to\n",
    "control formatting.\n",
    "\n",
    "Reading the file back can be done easily with the `loadtxt()`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0053390e",
   "metadata": {},
   "outputs": [],
   "source": [
    "pos = np.loadtxt('data.txt')\n",
    "# Check the data\n",
    "print(pos[:5, :])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "0607bb3c",
   "metadata": {},
   "source": [
    "Use the `unpack=True` argument to get back the data as\n",
    "columns.\n",
    "\n",
    "There are various ways to control the parsing of variables via\n",
    "`loadtxt()`. Check out the\n",
    "[documentation](https://numpy.org/doc/stable/reference/generated/numpy.loadtxt.html).\n",
    "\n",
    "# Efficiency\n",
    "\n",
    "First of all, a warning message:\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "492dd5f2",
   "metadata": {},
   "source": [
    "```\n",
    "We should forget about small efficiencies, say about 97% of the time:\n",
    "premature optimization is the root of all evil\n",
    "                                                 - Donald Knuth\n",
    "```"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "dc8a0e4d",
   "metadata": {},
   "source": [
    "\n",
    "In general, three levels:\n",
    "\n",
    "1.  Algorithms\n",
    "2.  Optimization\n",
    "3.  Parallelism\n",
    "\n",
    "The key to efficient computing with numpy is **operating on entire\n",
    "arrays, or array slices, at once**. The reason is that accessing\n",
    "individual array elements incurs in a significant **overhead**. It is\n",
    "crucial that the arrays or array slices are **large enough** to overcome\n",
    "the overhead of operating on them!\n",
    "\n",
    "We consider here a few simple examples, revisiting the calculation of\n",
    "the nano-particle CM. We can **measure the time** spent by the code with\n",
    "the `time` module, adding these lines of code at the\n",
    "beginning and at the end of each block.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b829d636",
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "ti = time.time()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "121ccaa9",
   "metadata": {},
   "outputs": [],
   "source": [
    "tf = time.time()\n",
    "print('Elapsed time: {:.2f} s'.format(tf-ti))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "dcd89979",
   "metadata": {},
   "source": [
    "These lines of codes will be stripped from this page for clarity. In a\n",
    "Jupyter notebook, add the `%%time` magic line at the\n",
    "beginning of each code cell you want to time. Some IDEs provide timing\n",
    "info of code blocks out of the box.\n",
    "\n",
    "## Reduction operations\n",
    "\n",
    "We reconsider the calculation of the total mass of the system. We repeat\n",
    "the operation multiple time to increase the computation time (not need\n",
    "with `%%timeit` magic)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1273f234",
   "metadata": {},
   "source": [
    "**Note**:\n",
    "Operations (sum, product, etc) that output a scalar from an array are called \"reduction\" operations."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "82a2fd4f",
   "metadata": {},
   "source": [
    "### Small arrays\n",
    "\n",
    "We start with the small array we created above ($N=3$).\n",
    "\n",
    "1.  We iterate over the individual array elements\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7d68fe62",
   "metadata": {},
   "outputs": [],
   "source": [
    "for _ in range(1000000):\n",
    "    tot = 0.0\n",
    "    for i in range(len(mass)):\n",
    "        tot += mass[i]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "b7243c4f",
   "metadata": {},
   "source": [
    "1.  Using the `sum` Python intrinsic function takes more time\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3924d942",
   "metadata": {},
   "outputs": [],
   "source": [
    "for _ in range(1000000):\n",
    "    sum(mass)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "0719ec8b",
   "metadata": {},
   "source": [
    "1.  Use the `numpy.sum` function\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4aa5cddf",
   "metadata": {},
   "outputs": [],
   "source": [
    "for _ in range(1000000):\n",
    "    np.sum(mass)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "7c3c9fa2",
   "metadata": {},
   "source": [
    "Ops, the numpy function is slower! That\\'s clear: the mass array is *too\n",
    "small* and the overhead of calling `sum` overwhelms the\n",
    "actual computing time.\n",
    "\n",
    "### Large arrays\n",
    "\n",
    "Let\\'s take a larger system, $N=10000$, and repeat the analysis\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7e480268",
   "metadata": {},
   "outputs": [],
   "source": [
    "mass = np.ones(10000)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "8c2d7c7a",
   "metadata": {},
   "source": [
    "1.  We iterate over the individual array elements\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f5d6341a",
   "metadata": {},
   "outputs": [],
   "source": [
    "for _ in range(1000):\n",
    "    tot = 0.0\n",
    "    for i in range(len(mass)):\n",
    "        tot += mass[i]"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "3a121bbf",
   "metadata": {},
   "source": [
    "1.  Using the `sum` Python intrinsic function now is faster\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b6ff88ef",
   "metadata": {},
   "outputs": [],
   "source": [
    "for _ in range(1000):\n",
    "    sum(mass)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "f8b9e08d",
   "metadata": {},
   "source": [
    "1.  Use the `numpy.sum` function...\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eb5406a5",
   "metadata": {},
   "outputs": [],
   "source": [
    "for _ in range(1000):\n",
    "    np.sum(mass)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "9a15beee",
   "metadata": {},
   "source": [
    "BOOM! With *large enough* arrays, the `numpy.sum()` function\n",
    "becomes a few orders of magnitude faster!\n",
    "\n",
    "## Vectorized operations\n",
    "\n",
    "Lists are very powerful and flexible data structures, but they are too\n",
    "slow for number crunching calculations: accessing the entries of a list\n",
    "is pretty inefficient. The same holds for numpy arrays, **unless we\n",
    "operate on large arrays or array sections**. This can be done by using\n",
    "the \\\"vector-syntax\\\" familiar to Fortran programmers.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1138ced4",
   "metadata": {},
   "source": [
    "**Note**:\n",
    "In the numpy jargon, operations carried out on array sections are said to be \"vectorized\" (not to be confused with [[https://en.wikipedia.org/wiki/Single_instruction,_multiple_data][SIMD vectorization]])."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "78be3c99",
   "metadata": {},
   "source": [
    "We revisit the center of mass calculation, using a large system this\n",
    "time\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32246f9a",
   "metadata": {},
   "outputs": [],
   "source": [
    "N, ndim = 10000, 3\n",
    "mass = np.ones(N)\n",
    "pos = np.zeros((N, ndim))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "c30809a0",
   "metadata": {},
   "source": [
    "This this is original version of our code\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4b04e293",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Repeat the inner loop many times and time the execution\n",
    "for _ in range(100):\n",
    "    # Compute CM\n",
    "    cm = np.zeros(ndim)\n",
    "    for i in range(pos.shape[0]):\n",
    "        cm += pos[i, :] * mass[i]\n",
    "    cm /= sum(mass)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "50571d1e",
   "metadata": {},
   "source": [
    "What if we had done the inner sum iterating over individual array\n",
    "elements?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cfe4ab98",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Repeat the inner loop many times and time the execution\n",
    "for _ in range(100):\n",
    "    # Compute CM\n",
    "    cm = np.zeros(ndim)\n",
    "    for i in range(pos.shape[0]):\n",
    "        # Explicit loop\n",
    "        for j in range(pos.shape[1]):\n",
    "            cm[j] += pos[i, j] * mass[i]\n",
    "    cm /= sum(mass)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "25b1bcec",
   "metadata": {},
   "source": [
    "It is not much slower, just a little bit. That\\'s because the slice\n",
    "`pos[i, :]` is small, hence the overhead is still\n",
    "significant.\n",
    "\n",
    "To optimize the code using plain `numpy,` we must sum the\n",
    "individual coordinates operating on larger array portions. We need the\n",
    "product of the $j$ cartesian coordinate times the mass.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "98ff53ad",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Repeat the inner loop many times and time the execution\n",
    "for _ in range(100):\n",
    "    # Compute CM\n",
    "    cm = np.zeros(ndim)\n",
    "    for j in range(pos.shape[1]):\n",
    "        cm[j] = np.sum(pos[:, j] * mass[:])\n",
    "    cm /= sum(mass)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "9ce6aa7f",
   "metadata": {},
   "source": [
    "That\\'s much better now! The operation `pos[:, j] * mass[i]`\n",
    "is \\\"vectorized\\\" over a large array portion and the sum incurs in\n",
    "little overhead (only three calls).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e1b28561",
   "metadata": {},
   "source": [
    "**Warning**:\n",
    "This time the modification to make our code efficient was pretty simple. However, achieving numpy vectorization may require rewriting our algorithm and sometimes this makes it less readable / less natural."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "dec56c02",
   "metadata": {},
   "source": [
    "# Visualization\n",
    "\n",
    "To visualize the nanoparticle you can try with [the matplotlib scatter\n",
    "function](https://matplotlib.org/stable/gallery/mplot3d/scatter3d.html),\n",
    "but it won\\'t be very nice. An alternative visualization package, which\n",
    "works well in a jupyter notebook, is\n",
    "[py3dmol](https://pypi.org/project/py3Dmol/).\n",
    "\n",
    "To install py3dmol in your virtual environment\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5d8d6548",
   "metadata": {},
   "outputs": [],
   "source": [
    "! pip install py3dmol"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "abaf47f4",
   "metadata": {},
   "source": [
    "You can now visualize configurations from a jupyter notebook with the\n",
    "following code. The input `positions` must be provided as a\n",
    "list of lists or as a 2-dim array with layout `(N, 3)`, where\n",
    "`N` is the number of particles.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "91a6fd6f",
   "metadata": {},
   "outputs": [],
   "source": [
    "def visualize(positions, colors=None, radii=None, color='white', radius=0.5):\n",
    "    \"\"\"\n",
    "    Visualize a particle configuration using 3dmol http://3dmol.csb.pitt.edu/\n",
    "    \"\"\"\n",
    "    import py3Dmol\n",
    "    if colors is None:\n",
    "        colors = [color] * len(positions)\n",
    "    if radii is None:\n",
    "        radii = [radius] * len(positions)\n",
    "    view = py3Dmol.view()\n",
    "    view.setBackgroundColor('white')\n",
    "    for i in range(len(positions)):\n",
    "        view.addSphere({'center': {'x': positions[i][0],\n",
    "                                   'y': positions[i][1],\n",
    "                                   'z': positions[i][2]},\n",
    "                        'radius': radii[i], 'color': colors[i]})\n",
    "    return view\n",
    "\n",
    "visualize([[0, 0, 0], [1, 0, 0]])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "daec7e99",
   "metadata": {},
   "source": [
    "If the configuration is not in `(N,3)` layout, just transpose\n",
    "the array before passing it to the function.\n",
    "\n",
    "# Exercise\n",
    "\n",
    "You are now ready to use **numpy arrays** to code the **nano-particle**\n",
    "setup! No need to worry much about efficiency at this stage. Just code\n",
    "it using the features of numpy arrays we have seen above. Encapsulate\n",
    "the code in functions to make them reusable, one for the setup of the\n",
    "lattice\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "186ef675",
   "metadata": {},
   "outputs": [],
   "source": [
    "def lattice(M, a=1):\n",
    "    \"\"\"\n",
    "    Return a numpy array with particles' position on a lattice of\n",
    "    spacing `a`, with `M` cells per side\n",
    "    \"\"\"\n",
    "    # your code here"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "83317fe7",
   "metadata": {},
   "source": [
    "another function to compute the center of mass\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a1a3e171",
   "metadata": {},
   "outputs": [],
   "source": [
    "def center_or_mass(position, mass):\n",
    "    \"\"\"\n",
    "    Return the center of mass of a system of particles \n",
    "    - at positions `position` [(N, ndim)-dimensional array]\n",
    "    - with masses `mass` [N-dimensional array]    \n",
    "    \"\"\"\n",
    "    # your code here"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "b464c81f",
   "metadata": {},
   "source": [
    "and finally a function to produce the nano-particle (which will\n",
    "internally call `lattice()` and\n",
    "`center_of_mass()`)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1acf26a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "def nano_particle(radius=3.0):\n",
    "    \"\"\"\n",
    "    Return the positions of a nanoparticle of given `radius`.\n",
    "    \"\"\"\n",
    "    # your code here"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "1d1805b8",
   "metadata": {},
   "source": [
    "Visualize the nanoparticle with the `visualize` function\n",
    "above to make sure it looks as expected!\n",
    "\n",
    "# Note\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b7d1b06d",
   "metadata": {},
   "source": [
    "```\n",
    "program main\n",
    "  real(8) :: x(3)\n",
    "  x = 1.0\n",
    "end program\n",
    "```\n"
   ]
  }
 ],
 "metadata": {},
 "nbformat": 4,
 "nbformat_minor": 5
}
