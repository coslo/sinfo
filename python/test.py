def main(verbose=False):
    if verbose:
        return 'Hello world!'

if __name__ == '__main__':
    from argh import dispatch_command
    dispatch_command(main)
