! Write a verse of a repetitive children's song
program main
  implicit none
  character(len=32) :: arg = "10"
  character(len=1) :: s = "s"
  integer :: i, stat
  ! Read the first argument on the command line, if present
  if (iargc() == 1) call getarg(1, arg)
  ! Store character variable arg in the integer variable i
  read(arg,*,iostat=stat) i
  if (i == 1) s = ""
  write(*,"(i0,a,a,a)") i, " green bottle", trim(s) ," hanging on the wall"
  write(*,"(a)") "but if one green bottle should accidentally fall"
  write(*,"(a,i0,a)") "there'll be ", i-1, " green bottles hanging on the wall"
end program main
