#!/bin/bash
# Compile the source code
path=$1
bin=$(basename $path .f90).x
gfortran -o $bin $path

# Execute the program
for n in $(seq 3 -1 1); do
    ./$bin $n
done
