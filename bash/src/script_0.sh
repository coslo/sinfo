#!/bin/bash
# Compile the source code
gfortran -o bottles.x bottles.f90

# Execute the program
for n in $(seq 3 -1 1); do
    ./bottles.x $n
done
