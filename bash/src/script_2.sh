#!/bin/bash
# Compile a fortran source code and execute the program
# with a range of different input arguments
if [ -z $path ] ; then
    echo Usage: $(basename $0) <path>
    echo Please provide the <path> to an source file
    exit 1
fi

# Compile the source code
bin=$(basename $path .f90).x
gfortran -o $bin $path

# Execute the program
if [ $? == 0 ] ; then
    for n in $(seq 10 -1 1); do
	./$bin $n
    done
fi
