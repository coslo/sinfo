#+title: Git, github, gitlab and all that
#+setupfile: ../org.setup
#+setupfile: /home/coslo/usr/org-config/doc.setup
#+latex_class_options: [10pt,a4paper]
#+options: toc:nil num:nil
#+property: header-args:sh :exports both :results verbatim :shebang #!/bin/bash :comments org :padline no :session bash
#+property: header-args:bash :exports both :results verbatim :shebang #!/bin/bash :comments no :padline no :session *Bash*

* Version control with git

** Useful links

Authoritative sources of information on git:
- The /official/ git documentation: https://git-scm.com/doc
- Beginner tutorial by /gitlab/: https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html
- Beginner tutorial by /github/: https://docs.github.com/en/github/getting-started-with-github/quickstart
- Questions on /stack overflow/: https://stackoverflow.com/questions/tagged/git

** Git in a nutshell

[[./Git-logo.png]]

Git is a *version control system* (VCS)

- Keep *history* of modifications to files
- Single-user or team *software development*
- Created by Linus Torvalds in 2005 to support the linux kernel development
- As of today, the *most popular* VCS out there

It is a /command line tool/. Check if you have it:
#+begin_src sh
git --version
#+end_src

#+results:
: 
: git version 2.34.1

*Found it?* Cool, then read on! If not: https://git-scm.com/downloads

** What do /I/ use it for?

Over the past 10+ years:
- *code development*
- *configuration files* (my =.bashrc=, =.emacs=, etc.)
- *research papers* - with /geek/ colleagues only :-(
- website, curriculum, tutorials, notebooks, ...

All this can be done
- as a *single user* or in *collaboration* with others
- both *offline* on a machine (ex. my laptop) or...
- ... over a *network* (I currently use https://framagit.org/ as main server)

** What will /you/ use it for, today?

- *Keep a text document* (code, report, ...) under version control
- Fine tune *git configuration*
- Edit a text document *collaboratively*
  
** Quick start

To keep track of the modification history of the files in a folder, turn that folder into a git *repository*.

Increments in the modification history appear as =commits=: each commit stores a set of modifications to the project, like *adding*, *modifying* or *removing* files.

Create a new folder, say =project/,= cd into it and initialize the repository
#+begin_src sh sh
mkdir project
cd project
git init
#+end_src
#+results:
#+begin_example

hint: Using 'master' as the name for the initial branch. This default branch name
hint: is subject to change. To configure the initial branch name to use in all
hint: of your new repositories, which will suppress this warning, call:
hint:
hint: 
hint: Names commonly chosen instead of 'master' are 'main', 'trunk' and
hint: 'development'. The just-created branch can be renamed via this command:
hint:
Initialized empty Git repository in /home/coslo/teaching/sinfo/git/project/.git/
#+end_example

#+attr_rst: :directive note
#+begin_quote
The history of your repository, along with its configuration, are stored in the =.git/= folder. If you delete that folder, the git repository is gone!
#+end_quote

Add a README file in the =project= folder
#+begin_src sh
echo "Hello World!" > README.md
git status
#+end_src
#+results:
#+begin_example

On branch master

No commits yet

Untracked files:
..." to include in what will be committed)
	README.md

nothing added to commit but untracked files present (use "git add" to track)
#+end_example

Now add the file to the project and commit the change
#+begin_src sh
git add .  # the dot will add all new files in the folder
git commit -m "Initial commit"
#+end_src

#+results:
: 
: [master (root-commit) e437035] Initial commit
:  1 file changed, 1 insertion(+)
:  create mode 100644 README.md

** More on commits

As you can see, this is done in two steps:
- =add=: include the new file into the list of modifications to record
- =commit=: actually store the modifications in the repository

The =-m= flag allows you to leave a "commit message" to describe the change.

Modify the file and check the difference
#+begin_src sh
echo "Hello World, again!" >> README.md
git diff
#+end_src
#+results:
: diff --git a/README.md b/README.md
: index 980a0d5..5eb476f 100644
: --- a/README.md
: +++ b/README.md
: @@ -1 +1,2 @@
:  Hello World!
: +Hello World, again!

Commit the change
#+begin_src sh
git commit -am "Update README"
#+end_src
#+results:
: [master ccd8fd8] Update README
:  1 file changed, 1 insertion(+)

If we modify some files, we can "add" the change and commit in one shot with the =-a= flag.

Have a look at what we have done so far
#+begin_src sh
git log
#+end_src

#+results:
#+begin_example
commit ccd8fd808ef882564819f26178a2d46b78871600 (HEAD -> master)
Author: Daniele Coslovich <92140-coslo@users.noreply.framagit.org>
Date:   Mon Oct 9 18:14:43 2023 +0200

    Update README

commit e437035bbf22baa2c08270be7da2df4cefc26b6f
Author: Daniele Coslovich <92140-coslo@users.noreply.framagit.org>
Date:   Mon Oct 9 18:14:16 2023 +0200

    Initial commit
#+end_example

Finally, removing a file is similar to adding it
#+begin_src sh
touch useless_file.txt
git add useless_file.txt
git commit -m "Add useless file"

git rm useless_file.txt
git commit -m "Remove useless file"
#+end_src

#+results:
: 
: [master 11c358d] Add useless file
:  1 file changed, 0 insertions(+), 0 deletions(-)
:  create mode 100644 useless_file.txt
: rm 'useless_file.txt'
: [master 34e35c8] Remove useless file
:  1 file changed, 0 insertions(+), 0 deletions(-)
:  delete mode 100644 useless_file.txt

*Moral #1*: Useful commands to check the status of your repository are
- =git status=
- =git diff=
- =git log=

*Moral #2*: Basic commands to add / remove files and commit changes
- =git add=
- =git rm=
- =git commit=

** Fine-tuning

*** Personal information

This is not necessary, but you may want to specify your name and e-mail (actually, git will prompt you to do that on the first commit)
#+begin_src bash :eval no
git config user.name <your_name>
git config user.email <your_email>
#+end_src
Add the =--global= flag to apply these settings globally for all your repositories

#+attr_rst: :directive warning
#+begin_quote
/Privacy notes/:
- If you plan to share your project publicly, consider using a dummy (inexistent) email account
- git commits may reveal a lot of information about a user's habits...
#+end_quote

*** Existing files

What if the folder /already/ contains files and/or sub-folders and we want to some (but not all) of them to the repository?

/Option 1/: add all the files then remove the ones you do not need
#+begin_src bash :eval no
git add .
git rm <not_needed_file>
git commit -m "Add files"
#+end_src

/Option 2/: add selected files
#+begin_src bash :eval no
git add <needed_file_1> <needed_file_2>
git commit -m "Add files"
#+end_src

You can use the star syntax (ex. =*.tex=) to match multiple file

*** Ignoring files

You may want to avoid storing certain files in your git repository, for instance
- temporary or backup files (=*.~=, =*.bak=)
- compilation artifacts and executables (=*.o=, =*.so=, =*.x=)

Create a text file called =.gitignore= at the root of your repositoriy and fill it with patterns matching the files to ignore
#+begin_src sh
cat > .gitignore <<EOF
*~
*.o
EOF

git add .gitignore
git commit -am "Add gitignore"
#+end_src

#+results:
: 
: [master c5decd3] Add gitignore
:  1 file changed, 2 insertions(+)
:  create mode 100644 .gitignore

From now on, =git status= will ignore these files and they will not be considered for commit

** FAQ

*** How do I go back in time?

First look for the /hash/ of the commit you are interested in

#+begin_src sh
git log --pretty=oneline
#+end_src

#+results:
: c5decd31ecc1dcfafe280d6587bacda7feacdb8d (HEAD -> master) Add gitignore
: 34e35c885b157e3160c60ba441a233e6c188fca1 Remove useless file
: 11c358de502f9056402d27ac169604876b14146f Add useless file
: ccd8fd808ef882564819f26178a2d46b78871600 Update README
: e437035bbf22baa2c08270be7da2df4cefc26b6f Initial commit

Check out a copy of the project as it was in that commit
#+begin_src bash :eval no
git checkout c5decd
#+end_src

To get back to the current state of the project
#+begin_src bash :eval no
git checkout master
#+end_src

*** How can I undo the last commit

This is one of the [[https://stackoverflow.com/questions/927358/how-do-i-undo-the-most-recent-local-commits-in-git][most popular question]] about git on stackoverflow.
#+begin_src sh
git commit -m "Something terribly misguided"
git reset HEAD~
#+end_src

Then edit the files as necessary and commit again.

*** Do I /really/ need to write those commit messages?

Writing commit messages may seem a burden (and actually, you can leave them blank) but it /is/ important.

#+attr_rst: :directive hint
#+begin_quote
You may be able to write commit messages directly from your IDE and/or text editor.
#+end_quote

[[./git_commit_2x.png]]

There are well-established guidelines to write *good* commit messages, so that they are informative and easy to read
https://cbea.ms/posts/git-commit/

*Structure*
- /Separate subject from body with a blank line/
- Use the body to explain what and why vs. how

*Style*
- /Use the imperative mood in the subject line/
- Capitalize the subject line
- Do not end the subject line with a period
- Limit the subject line to 50 characters
- Wrap the body at 72 characters

#+attr_rst: :directive hint
#+begin_quote
Write "Fix typo in tutorial", not "fixed typo in tutorial."
#+end_quote

** Software versioning

For basic usage, /explicit/ versioning is not necessary: use git hashes to identify commits. However, it is /crucial/ for libraries and API, or any code on which /other/ codes may depend upon.

- =X=: simple incremental versioning
- =X.Y=: major-minor (ex: =0.1=, that's where you start from)
- =X.Y.Z=: major-minor-patch (ex. git =2.17.1=)
- =X.Y.Z.*=: alpha / beta / release candidate info

One of the most sensible ways to increment versions is [[https://semver.org][semantic versioning]].

In a nutshell:
- increase =X=: *backward incompatible changes*
- increase =Y=: new features
- increase =Z=: /bug fixes, cosmetics, optimization/
  
Explicit versioning in git is handled via =tags=. 

Tags are snapshots of the state of a project to which git assigns a meaningful identifier

#+begin_src bash :session s
git commit -m "Bump version 0.1.0" 
git tag -am "0.1.0" 0.1.0
#+end_src

List all tags
#+begin_src bash :session s
git tag -l
#+end_src

#+results:
: 0.1.0

Check out the project at a specific tag
#+begin_src sh
git checkout 0.1.0
#+end_src

Use =git checkout master= to get back to the current state of the project.

** Branches

Branches allow you to keep *multiple versions of your project* in parallel.

Repositories are created with a default branch, typically called *master* or *main*. That's the branch to which we committed the modifications above.

Typical use case: develop a new feature in a separate /feature branch/, then merge it in the master branch.
#+begin_src ditaa :file branch1.png
--*---*--*--+----*--*---+--*--*--*-->  master
            |           ^
            |           |merge
            |           |
            \--*--*--*--/           
        feature branch
#+end_src

#+results:
[[file:branch1.png]]

Glossary:
- /Fast-forward/: no commits between the creation of the new branch and the merge
- /Conflict/: concurrent modification to the same part of a file

Create a new branch and check it out
#+begin_src sh
git checkout -b new_cool_feature
# commit commit commit ...
#+end_src
#+results:
: Switched to a new branch 'new_cool_feature'

Show available branches
#+begin_src sh
git branch -va
#+end_src
#+results:
:  master            c5decd3 Add gitignore
: * new_cool_feature c5decd3 Add gitignore

The prompt of your terminal should tell you which branch you are working on
#+begin_example
coslo@local:~/teaching/sinfo/git/project/(new_cool_feature)$
#+end_example

If not, you may want to follow these instructions https://blog.sellorm.com/2020/01/13/add-the-current-git-branch-to-your-bash-prompt/

When the new feature is ready, get back to the master branch and merge the feature branch
#+begin_src sh
git checkout master
git merge new_cool_feature
#+end_src

#+results:
: Switched to branch 'master'
: Already up to date.

If *conflicts* occur, you will have to modify the files to remove them. =git status= will tell you how to proceed.

#+attr_rst: :directive warning
#+begin_quote
Beware! Conflict handling can be nasty...
#+end_quote

** Exercise: thesis in latex

- Make sure =git= is installed on your machine (if not, follow https://git-scm.com/downloads)
- Download and extract the latex template from [[https://framagit.org/coslo/template-latex/][this link]] (do not clone the repository)
- Turn it into a new git repository in a folder called =thesis=: add all the relevant files and commit your changes
- Familiarize yourself with the commands seen above (except those related to branching, unless you are already confident with the git basics)

* Collaborative editing

** Remote repositories

Git is a *distributed VCS*: several copies of the same repository may exist and exchange with one another.

Multiple instances of a project may exist:
- *locally* on a machine
- over a *network*, communicating through various protocols (ssh, git, https)

From the perspective of a given repository, other repositories of the same project are called *remotes*. Once remotes are set up, the workflow is identical whether the repository copies are on the same hard-disk or on a different machine.

Typically, one keeps a repository as an "official" source for the project. This official repository must be a =bare= one: there are no checked out files in it.

*Example*:
- all repositories are "local", i.e. on the same disk
- two repositories cloned from =project=
- =project_1= is a remote of =project_2=, named =extra=

#+begin_src ditaa :file dia0.png
+-----------+            +---------+            +-----------+
|   local   |            |  local  |            |   local   |
+-----------+            +---------+            +-----------+
|           |   origin   |         |   origin   |           |
| project_1 |----------->+ project +<-----------| project_2 |
|           |            |cBLU     |            |           |
+----+------+            +---------+            +----+------+
#+end_src
#+results:
[[file:dia0.png]]

Initialize a bare "official" repository in the =project= folder and clone two copies
#+begin_src sh
git init --bare project
git clone project project_1
git clone project project_2
#+end_src

#+results:
#+begin_example
Initialized empty Git repository in /home/coslo/teaching/sinfo/git/project/
Cloning into 'project_1'...
warning: You appear to have cloned an empty repository.
done.
Cloning into 'project_2'...
warning: You appear to have cloned an empty repository.
done.
#+end_example

#+begin_src ditaa :file dia0b.png
+-----------+            +---------+            +-----------+
|   local   |            |  local  |            |   local   |
+-----------+            +---------+            +-----------+
|           |   origin   |         |   origin   |           |
| project_1 |----------->+ project +<-----------| project_2 |
|           |            |cBLU     |            |           |
+----+------+            +---------+            +----+------+
     ^				                     |
     |                                               |
     +-----------------------------------------------+
                           extra
#+end_src
#+results:
[[file:dia0b.png]]


Add =project_1= as a remote of =project_2=
#+begin_src sh
cd project_2
git remote add extra ../project_1
#+end_src

#+results:

The syntax is =git remote add <remote_name> <remote_path>=
- =<remote_name>= is a name for the remote
- =<remote_path>= is the path to the remote (can be over the network, see below)

Have a look at the remote repositories we have access to
#+begin_src sh
git remote -v show
#+end_src

#+results:
: extra	../project_1 (fetch)
: extra	../project_1 (push)
: origin	/home/coslo/teaching/sinfo/git/project (fetch)
: origin	/home/coslo/teaching/sinfo/git/project (push)

By default, the =origin= remote is the one we cloned the project from.

We can now check out any branch of the =project_1= remote repository, inspect or grab files from them, merge them into our own branches.

*Example*: the master branch of =project_1= contains some interesting new commits and we want to merge them in our code:
#+begin_src bash :eval no
git pull extra master
git checkout extra/master
# ... look at code here

# Get back to our master branch and merge the contect of extra/master
git checkout master
git merge extra/master

# To keep a flat commit history, instead of merge use
git rebase extra/master
#+end_src

** Remote collaboration

To *collaborate remotely*, users must have access to a git server. A git server may boil down to a simple ssh server, to which you and your collaborators have access to, or using a proper git server (with =git= protocol).
#+begin_src ditaa :file images/dia2.png
+-----------+          +---------+          +-----------+
|   pc 1    |          | server  |          |    pc 2   |
+-----------+          +---------+          +-----------+
|           |  origin  |         |  origin  |           |
| project_1 +----=---->+ project +<----=----+ project_1 |
|cGRE       |          | cBLU    |          |cYEL       |
+-----------+          +----+----+          +-----------+
                            ^
                            :origin
+-----+-----+               |  
|   pc 1    |               |
+-----------+---------------/
|           |
| project_2 |
|cGRE       |
+-----------+
#+end_src
#+results:
[[file:images/dia2.png]]

The power of git allows you to deal with multiple remote repositories on /different/ servers. Say Alice and Bob both cloned a project from a server. They can push this project to their respective git servers (github and gitlab).
#+begin_src ditaa :file dia3.png
                 +----+----+
                 | server  |
                 +---------+
                 |         |
           +---->+ project +<----+
           :     | cBLU    |     :
           |     +----+----+     |
           |                     |
    +------+-----+         +-----+------+            
    |  Alice pc  |         |   Bob pc   |
    +------------+         +------------+
    |            |         |            |
    |  project   |         |  project   |
    |cGRE        |         |cYEL        |
    +------------+         +------------+
	   |                      |
           |origin                |origin	  
           :                      :
           V	                  v	  
      +----+----+            +----+----+	  
      | gitlab  |     	     | github  |	  
      +---------+     	     +---------+	  
      |         |     	     |         |	  
      + project +	     + project +	  
      | cBLU    |	     | cBLU    |	  
      +----+----+	     +----+----+	  
#+end_src
#+results:
[[file:dia3.png]]


Then, Alice adds Bob's repository, which is stored on github, as remote...
#+begin_src bash :eval no
# On Alice PC
git remote add bob https://github.com:bob/project.git
#+end_src

...and viceversa!
#+begin_src bash :eval no
# On Bob PC
git remote add alice https://gitlab.com:alice/project.git
#+end_src

#+begin_src ditaa :file dia4.png
      +-----------+          +-----------+	  
      |  gitlab   |    	     |  github   |	  
      +-----------+   	     +-----------+	  
      |           |    	     |           |	  
+---->+ project   |	     + project   +<---+  
|     | cBLU      |	     | cBLU      |    |	  
|     +----+------+	     +----+------+    |	  
|	   ^                      ^	      |
|          |origin                |origin     |	  
|          :	                  :	      |  
|          |	                  |	      |
|    +------+------+         +-----+------+   |            
|    |  Alice pc   |         |   Bob pc   |   |
|    +-------------+         +------------+   |
|    |             |         |            |   |
|    |  project    |         |  project   |   |
|    | cGRE        |         | cYEL       |   |
|    +------+------+         +------+-----+   |
|           |                       |         |
|           |                       |         |
|           +--------------=--------|-----=---+
+----------------------------=------+      bob
 alice
#+end_src
#+results:
[[file:dia4.png]]

Pushing and pulling each others' project can then be done with the same commands seen above for local remotes
#+begin_src bash :eval no
git remote add new_remote git@<your_server>:<your_username>/<your_project>.git
git remote -v  # display remotes to check
git pull --rebase new_remote master  # fetch and rebase remote master into local master
#+end_src

** Github, gitlab and all that

git is a *decentralized* VCS: no central server, every repository can share with any other.

However, git servers have undergone a coarsening process and a few big *centralized* servers ("silos") have emerged.

Along this process, *web applications* grew around git servers, adding functionalities that allow users to browse repositories and collaborate on software:

- *github*: centralized, closed source, owned by Microsoft (https://github.com/explore)
- *gitlab*: centralized, open-source edition (https://gitlab.com/explore/projects/trending)
- self-hosted instances of gitlab: ex. *framagit* (https://framagit.org/)
- *gittea* (https://gitea.io/en-us/), *gogs* (https://gogs.io/): self-hosted
- *codeberg* (https://codeberg.org/): based on *forgejo*, which is itself a fork of *gittea*
- ActivityPub for federation
- ...

*Features*:

- *issue tracking*: report bugs or ask for new features
- *pull requests*: facilitate merging of new features and bug fixes
- *continuous integration*: automatic testing and deployment of code on multiple platforms
- *web page hosting*: host static web pages (ex. documentation)
- *social networking*: collaborate and discover new projects

*Anti-features*:

- social networking (!)
- walled gardens
- over-engineered

** Creating an account

Both gitlab and github have free plans for /private/ and /public/ repositories. Here I create an account on gitlab https://gitlab.com/users/sign_up (no need to give your actual identity)

[[./gitlab1.png]]

[[./gitlab2.png]]

** Setting up ssh keys

/If you do not have them yet/, create a pair of private-public ssh keys. This will encrypt your communications to the remote server using the =git= protocol
#+begin_src sh
ssh-keygen
# Press Enter three times... (or add a passphrase for security)
#+end_src

Your public key is
#+begin_src sh
cat ~/.ssh/id_rsa.pub
#+end_src

#+results:
: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDnnpooSARIJ6HYdy/QWrtv0C1XANNKWUIDei3BxCXy9f62hzAQMtJuVAr/
: wu0WfNnygcP1w+D/LOScdrRrzw5hCHainVcVQustw8OtkyW19ITboMD5KC9P2pdSPtsdaUNF8ZK0+fDfYwFpRfeF0+qsebQs
: OWaLF03x+o69ZnJf1Gs6PGER2aETBVz9UUniveMf0CqHGmUK5l5Yj9Otzs4uAAYEyvGPt5amtRWMGufQIB5edrOqa/+Mu8gI
: xu6uHUtLsEs9AzDslM/McWDVoDAumnBgJsLGJpD3/0DsD4dToH6NtRsNLhy8NmRotsJwL+KSIrT9zpVfk67 coslo@local

Your private key is
#+begin_src sh
cat ~/.ssh/id_rsa
#+end_src

#+attr_rst: :directive danger
#+begin_quote
Never ever share your private key!
#+end_quote

In the /SSH Keys/ settings on the web interface (top right corner, then /Preferences/), add a new key by pasting the content of =~/.ssh/id_rsa.pub=

[[./addssh.png]]

#+attr_rst: :directive note
#+begin_quote
It is also possible to have or grant access to a remote repository with *tokens* (both for [[https://docs.gitlab.com/ee/security/token_overview.html][gitlab]] and [[https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token][github]])
#+end_quote

** Creating a new project

You can push your own =thesis= git project directly to the gitlab server.

The default remote is called =origin=. Add the gitlab server as the new =origin= remote, then push the =master= branch over there
#+begin_src bash :eval no
git remote add origin git@<your_server>:<your_username>/thesis.git
git push -u origin master
#+end_src

#+attr_rst: :directive warning
#+begin_quote
Remember to change the dummy names ("your_server", "your_username") above with yours!
#+end_quote

To push all existing branches (and tags, if you have them)
#+begin_src bash :eval no
git push -u origin --all
git push -u origin --tags
#+end_src

** Differences between pull, merge and rebase

First, note that 
#+begin_src bash  :eval no
git pull origin master
#+end_src

is equivalent to
#+begin_src bash :eval no
git fetch origin
git merge origin/master
#+end_src

Unless the merges resolves in a /fast-forward/, =git= will keep track of the branching path in the commit history. To always flatten commit history, make a /rebase/ instead
#+begin_src bash :eval no
git pull --rebase origin master
#+end_src

or equivalently
#+begin_src bash :eval no
git fetch origin
git rebase origin/master
#+end_src

** Exercise: create an account on a git platform

#+begin_src ditaa :file ex2.png
      +------+------+            +----------------+     
      |    local    |	         |    framagit    |     
      +-------------+	         +----------------+     
      |             |    origin  |                |     
      |    thesis   |---=------->+ template-latex +     
      |cGRE         |	         | cBLU           |     
      +-------------+	         +---------+------+     
#+end_src

#+results:
[[file:ex2.png]]

- choose a web platform of your choice
- create an account on it
- create your ssh keys and add /the public one/ in your account
- create a new private project
- push your =thesis= repository over there
- browse your repository online
- /commit, commit, commit.../ then push the changes to the server again

To upload existing files from your computer, use the instructions below. Remember to replace dummy paths with relevant ones.

*** Push an existing Git repository

This is the most common use case
#+begin_src bash :eval no
cd existing_repo
git remote rename origin old-origin
git remote add origin git@<your_server>:<your_username>/thesis.git
git push -u origin --all  # -u sets origin as upstream server of every branch 
git push -u origin --tags  # if you have tags
#+end_src

*** Push an existing folder

If =existing_folder= contains a repository but you want to start a brand new one, just delete the =.git= folder at its root
#+begin_src bash :eval no
cd existing_folder
git init --initial-branch=main
git remote add origin git@<your_server>:<your_username>/thesis.git
git add .
git commit -m "Initial commit"
git push -u origin main
#+end_src

*** Create a brand new repository

This assumes you create an empty repository on the remote server
#+begin_src bash :eval no
git clone git@<your_server>:<your_username>/thesis.git
cd thesis
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
#+end_src

** Project: collaborative editing of a document

[[./dia4.png]]
- who plays Alice? who plays Bob?
- clone the repository =template-latex= from framagit
- create a new project on your remote server (either gitlab or github)
- set the =origin= of the repository you cloned to your new project on the remote server
- edit the =thesis.tex= file and commit the change
- push the change to your remote server
- add a new remote pointing to your collaborator's project on his/her remote server
- pull his/her commit into your own master branch

*Bonus*:
- handle conflicts if both of you modify the same parts of =thesis.tex=!

