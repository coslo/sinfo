#+title: Strumenti informatici per la fisica
#+setupfile: ./org.setup
#+property: header-args:sh :session bash

* TODO [2/21]                                                      :noexport:

Basics
- [X] tricks with jupyter (indent, comment)
- [X] mention julia
- first lesson was a bit long, no time to start the exercise in class this year 
- usual mess with different distributions/oses/IDEs: there is no way around
- [ ] split some cells into subcells for clarity
- [ ] absolutely: add diagram of references to objects
- [ ] explain id() once you have the diagram
  
- [ ] jupyter say once code is stable, move it to a module, then import
- [ ] complexity of CM and energy
- [ ] general strategy: interface in python, algo in fortran
- [ ] implement double loop with
  - [ ] python
  - [ ] numba
  - [ ] f2py (good to start)
  - [ ] f2py-jit (why? explain)
- [ ] interfacing
  - [ ] order of array
  - [ ] return final arguments
  - [ ] scalars inplace
- [ ] compile from command line to debug
- [ ] compare performance on real code
    
Later
    
- NumPy: Numerical Python
- timing code [solo cose di base, no profiling sistematico]
- f2py - interfacing Fortran code to Python

* Participants :noexport:

Da firmare:

Da consegnare
- [-] GIOVANNI TARTAGLIA	giovannitartaglia01@gmail.com: comp
- [-] COSTANZA LINCETTO	costanzalincetto@gmail.com: progetto computazionale (RW o Ising)

Missing:
- [ ] GIORGIA MUSSO	giorgiamussopd@gmail.com
- [ ] LIVIU CLAUDIU BERT VITAN	liviuclaudiubert.vitan@studenti.units.it

Firmati:
- [X] IACOPO RICCI	iakyricci.ir@gmail.com
- [X] GABRIELE BARTOLINI gabrielebartolini110@gmail.com: exp, analisi datai fit Voigt
- [X] FRANCESCA CUCCHIARO	cucchiarofrancesca@gmail.com
- [X] ELENA BABICI	elena.babici@studenti.units.it: exp, fit Igor vs python
- [X] FORMENTIN SIMONE SIMONE.FORMENTIN@studenti.units.it: exp, analisi dati in ipynb
- [X] CECILIA BOTTA	Botta.cecilia.01@gmail.com: comp
- [X] ALBERTO TUROLDO	ALBERTO.TUROLDO@studenti.units.it: comp  
- [X] FRANCESCO PERIOLI	12francesco34@gmail.com: f2py-jit + git
- [X] GABRIELE COLTRIOLI	gabry0211@gmail.com: exp, analisi dati con fit Voigt -> OK

* Syllabus :noexport:

** Contenuti

L'attività formativa verrà svolta in forma di tutorials, che presenteranno alcuni strumenti informatici di utilità generale in fisica. Si affronteranno in particolare: l'utilizzo efficiente del terminale e del linguaggio bash, elementi di programmazione in Python e suo interfacciamento con linguaggi di programmazione compilati, l'utilizzo di git per il controllo di revisione e lo sviluppo collaborativo di codici e di documenti scientifici.

- Utilizzo efficiente del terminale, bash scripting
- Introduzione a Python, Jupyter and matplotlib
- Calcolo numerico in Python con numpy
- Interfacciamento tra Python e Fortran con f2py
- Compilazione just-in-time con f2py-jit e numba
- Controllo di revisione con git, piattaforme web per git

The course will delivered through tutorial-like lectures, which will introduce some IT tools useful in physics. The course will cover in particular: efficient usage of the terminal and bash scripting; basic elements of the Python programming language; interfacing Python with compiled languages; git for version control and collaborative editing of codes and documents.

- Efficient usage of the terminal and bash scripting
- Introduction to Python, Jupyter and matplotlib
- Numerical computing in Python with numpy
- Interfacing Python and Fortran with f2py
- Just-in-time compilation with f2py-jit and numba
- Revision control with git, web platforms for git

** Obiettivi

D1 - Conoscenza e capacità di comprensione

Acquisire tecniche e metodi informatici di base utili alla simulazione numerica e/o all'analisi dei dati in fisica. 

D2 - Capacità di applicare conoscenza e comprensione

Saper applicare almeno uno degli strumenti presentati nei tutorials nel contesto di un progetto scientifico rilevante per la laurea magistrale in Fisica, ad esempio nel contesto del progetto finale del Laboratorio di fisica computazionale o del Laboratorio di fisica sperimentale della materia o di un altro progetto definito in accordo con il docente.

D5 - Capacità di apprendimento

Approfondire in modo autonomo gli argomenti trattati nel corso tramite la consultazione di materiali forniti dal docente e/o l'esecuzione di notebooks computazionali.

** Metodi

Lezioni frontali in forma di tutorial.

** Eval

I crediti si considereranno acquisiti (con idoneità, senza voto) dimostrando l’utilizzo di almeno uno degli strumenti presentati nel contesto di un progetto scientifico rilevante per la laurea magistrale in Fisica, ad esempio nel contesto del progetto finale del Laboratorio di fisica computazionale o del Laboratorio di fisica sperimentale della materia o di un altro progetto definito in accordo con il docente.

* Overview

This is a series of short informal tutorials on

1. efficient usage of the *terminal* and *bash* scripting
2. introduction to *Python*, *Jupyter* and *matplotlib*
3. computing in Python with *numpy*
4. interfacing *Python** and *Fortran* with *f2py* (vs. *numba*)
5. revision control with *git* (+ *github*, *gitlab* & all that)

The focus is on free and open source ([[https://en.wikipedia.org/wiki/Free_and_open-source_software][FOSS]]) software tools, both [[https://en.wikipedia.org/wiki/High-_and_low-level][low- and high-level]].

*Free* (as in "free speech") software can be inspected, modified and redistributed by anyone. It implies the code is *publicly* available. It does not imply it is free as in "free beer" - although it mostly is.

1) *High-level* tools provide *simple interfaces* to perform complex tasks. They are meant to be *easy to use* and *intuitive*, but they also tend to be *rigid*.

2) *Low-level* tools provide *fine control* on the building blocks needed to accomplish a task. They are *flexible* and *powerful*, but their *learning curve* is *steeper*.

#+begin_export rst
.. toctree::
   :hidden:
   :maxdepth: 1
   :glob:
      
   bash
   python
   numpy
   intermediate
   faster_python
   git
#+end_export

[[./tools.png]]

Web platforms and several IT services operate over a *network* (nodes connected by edge). Networks can be

- *centralized*: a single, central authority server to which all nodes must connect
- *decentralized*: no central authority, multiple /federated/ servers
- *distributed:* each node can exchange directly with any other

[[./networks.png]]

Decentralized and distributed networks tend to be more [[https://en.wikipedia.org/wiki/High_availability#Resilience][resilient]].

[[./chart.png]]
